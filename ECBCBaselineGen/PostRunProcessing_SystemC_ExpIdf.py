import os
from SystemAssignment import set_IDF
import shutil

def getExpIDF(IDF_P):
    folder = os.path.dirname(IDF_P)
    expended_Path = os.path.join(folder,"baseline.expidf")
    expIdf_Path =os.path.join(folder,"baselineExp.idf")
    shutil.copyfile(expended_Path, expIdf_Path)  
    ECBC_ExpStandard_Idf = set_IDF(expIdf_Path)
    return ECBC_ExpStandard_Idf

def System_C_Pump_Power(ECBC_ExpStandard_Idf, chiller_capacity_air_kwr, chiller_capacity_water_kwr):
    tot_chiller_capacity_kwr = chiller_capacity_air_kwr + chiller_capacity_water_kwr
    Pump_VariableSpeed_objs = ECBC_ExpStandard_Idf.idfobjects["Pump:VariableSpeed".upper()]
    Pump_VariableSpeed_obj = [obj for obj in Pump_VariableSpeed_objs if obj.Name == "Chilled Water Loop ChW Secondary Pump"]
    if Pump_VariableSpeed_obj:
        Pump_VariableSpeed_obj.Design_Power_Consumption = 9.1 * tot_chiller_capacity_kwr
        ECBC_ExpStandard_Idf.save()

    Pump_ConstantSpeed_objs = ECBC_ExpStandard_Idf.idfobjects["Pump:ConstantSpeed".upper()]
    for Pump_ConstantSpeed_obj in Pump_ConstantSpeed_objs:
        Pump_ConstantSpeed_obj.Design_Power_Consumption = 9.1 * chiller_capacity_kwr
        ECBC_ExpStandard_Idf.save()


def PostRun_ExpIdf(IDF_P, ):
    ECBC_ExpStandard_Idf = getExpIDF(IDF_P)
    

    