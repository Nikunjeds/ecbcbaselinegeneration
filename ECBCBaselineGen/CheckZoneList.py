"""helper function for checking Zonelist and Zonelist Name"""
#function to validate IDF for zonelist name 
def zonelistCheck(ECBC_standard_Idf,ECBC_Zonelist):
    #read all zonelist objects
    zonelist_objs = ECBC_standard_Idf.idfobjects["zonelist".upper()]
    #check if any zonelist object found or not
    ZoneListCount = len(zonelist_objs)
    
    zone_arr = []
    invalid_ZoneList = []
    #check if no zonelist in IDF file then Exit, else check for standard Zonelist Names
    if ZoneListCount == 0:
        return "Invalid IDF- Zonelist Not Found"
    else:
        #for every Zonelist objects
        for zonelist in zonelist_objs:
            zonelist_name = zonelist.Name
            # check if name of Zonelist object exist in Standard Zonelist names  store them in invalid_zonelist array      
            if not(zonelist_name in ECBC_Zonelist):
                invalid_ZoneList.append(zonelist_name)
        #if invalid_zonelist is not empty
        if invalid_ZoneList: # not empty check
                status_msg = "Invalid IDF - below mentioned zonelist name is not standard Zonelist name \n" + str(invalid_ZoneList)
                return status_msg              
                #return "Invalid IDF - " + zonelist_name + " is not standard Zonelist name"        
        unmapped_Zones = check_Zone_Zonelist_Mapping(ECBC_standard_Idf)
        if unmapped_Zones:
            return "Invalid IDF - Below mentioned zone not mapped to zonelist\n" + str(unmapped_Zones) 
        else:    
            return "Zonelist Check Completed Successfully"
    
#function to check zone oand zonelist mapping.. check if any zone is unassigned in zonelist    
def check_Zone_Zonelist_Mapping(ECBC_standard_Idf):
    unmapped_Zones=[]
    ZoneList_objs = ECBC_standard_Idf.idfobjects["zonelist".upper()]
    zone_arr = []
    for zonelist in ZoneList_objs:
     #   print zonelist.Name
        zones = [zonelist[fieldname]  for fieldname in zonelist.fieldnames if (fieldname.startswith("Zone") and zonelist[fieldname] != "")] 
        zone_arr.extend(zones)
 
    Zone_objs = ECBC_standard_Idf.idfobjects["zone".upper()]
    for zone in Zone_objs:
        if not(zone.Name in zone_arr):
            unmapped_Zones.append(zone.Name)
    return unmapped_Zones
    