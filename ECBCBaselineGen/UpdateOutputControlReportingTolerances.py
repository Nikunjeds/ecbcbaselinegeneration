"""helper function to update Output Control object""" 
#function to update outputControl_ReportingTolerance object in both Proposed and ECBC_standard Idf
def update_outputControl_ReportingTolerance(ECBC_standard_Idf):
    print "Updating OutputControl:ReportingTolerances Object..."
    #Search OutputControl:ReportingTolerances in proposed IDF
    outputControl_ReportingTolerance_obj = ECBC_standard_Idf.idfobjects["OutputControl:ReportingTolerances".upper()]
    #if object found copy same object to ECBC_standard_Idf
    if not outputControl_ReportingTolerance_obj:
        
        new_outputControl_ReportingTolerance_obj = ECBC_standard_Idf.newidfobject("OutputControl:ReportingTolerances".upper())
        new_outputControl_ReportingTolerance_obj.Tolerance_for_Time_Heating_Setpoint_Not_Met = 1
        new_outputControl_ReportingTolerance_obj.Tolerance_for_Time_Cooling_Setpoint_Not_Met = 1
        ECBC_standard_Idf.save()
        

    print "OutputControl:ReportingTolerances Object Updated."
    return ECBC_standard_Idf