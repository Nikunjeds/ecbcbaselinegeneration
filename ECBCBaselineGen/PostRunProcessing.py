"""Function that will run after running simulation - check on output csv and update ECBC_standard_Idf  """
from eppy import modeleditor
from eppy.modeleditor import IDF
import sys
import json

from ExtractTableData.Constants import get_constants

from eppy.results import readhtml
import pprint
def post_Run_Processing_System_B(Output_HTML, ECBC_standard_Idf):
    pp = pprint.PrettyPrinter()
    VRF_System = []
    
    filehandle = open(Output_HTML, 'r').read() # get a file handle to the html file
    htables = readhtml.titletable(filehandle) # reads the tables with 
    tbl =  [htable for htable in htables if htable[0] in ["AirConditioner:VariableRefrigerantFlow"]] 
    #tbl represent table with header- tbl[0][1] gives table data
    print  "Reading AirConditioner:VariableRefrigerantFlow table..."
#    h_table = readhtml.named_grid_h(tbl[0][1])
    #print tbl
    a_tbl = tbl[0][1]
#    print h_table
#    print a_tbl[1][1]
#    print a_tbl[0][1]
    for x in range(1,len(a_tbl)):
        VRF_System = a_tbl[x][0]
        VRF_System = VRF_System.replace(' VRF HEAT PUMP','')
        CoolingCOP = a_tbl[x][1]
        CoolingCOP_KW = CoolingCOP/1000
        CoolingEER = 0
        #print VRF_System + ' ' + str(CoolingCOP_KW)
        tbl_rows = get_constants(table='Tbl_5_1_Eff',Type='VRF_A', Compliance='ECBC')
        for y in range(0,len(tbl_rows)):           
            if tbl_rows[y]["Max_kWr_"] != '':
                if (CoolingCOP_KW > float(tbl_rows[y]["Min_kWr_"]) and CoolingCOP_KW <= float(tbl_rows[y]["Max_kWr_"])):
#                    print tbl_rows[y]["Min_kWr_"]
#                    print tbl_rows[y]["Max_kWr_"]
                    CoolingEER = tbl_rows[y]["EER"]
                    break
            else:
                if (CoolingCOP_KW > float(tbl_rows[y]["Min_kWr_"])):
#                    print tbl_rows[y]["Min_kWr_"]
#                    print tbl_rows[y]["Max_kWr_"]
                    CoolingEER = tbl_rows[y]["EER"]
                    break
        print VRF_System  + ' ' + str(CoolingCOP_KW) + ' ' + str(CoolingEER)    
        VRF_System_objs = ECBC_standard_Idf.idfobjects["HVACTemplate:System:VRF".upper()]
        VRF_System_obj = [VRF_Systems for VRF_Systems in VRF_System_objs if VRF_Systems.Name.upper() == VRF_System.upper()]
        #print VRF_System_obj[0]
        VRF_System_obj[0].Gross_Rated_Cooling_COP = CoolingEER
        VRF_System_obj[0].Gross_Rated_Heating_COP = CoolingEER
        
        print VRF_System  + " Modified"
        ECBC_standard_Idf.save()       
        
def post_Run_Processing_System_A(Output_HTML, ECBC_standard_Idf):
    filehandle = open(Output_HTML, 'r').read() # get a file handle to the html file
    htables = readhtml.titletable(filehandle) # reads the tables with 
    tbl =  [htable for htable in htables if htable[0] in ["Cooling Coils"]] 
    print  "Reading Cooling Coils table..."
    a_tbl = tbl[0][1]
    #print a_tbl

    for x in range(1,len(a_tbl)):
        Coil_Name = a_tbl[x][0]
        if Coil_Name.find('PTHP') != -1:
            Coil_Name = Coil_Name.replace(' PTHP COOLING COIL','')       
            Nominal_Total_Capacity = a_tbl[x][3]       
            Nominal_Total_Capacity_KW = Nominal_Total_Capacity/1000
            Cooling_Fan_Power =  0.04152 * Nominal_Total_Capacity_KW
            CoolingCOP = 0
            tbl_rows = get_constants(table='Tbl_5_1_Eff',Type='USPAC_A', Compliance='ECBC')

    #        print Nominal_Total_Capacity_KW
    #        print tbl_rows
            for y in range(0,len(tbl_rows)): 
                if tbl_rows[y]["Max_kWr_"] != '':
                    if (Nominal_Total_Capacity_KW > float(tbl_rows[y]["Min_kWr_"]) and Nominal_Total_Capacity_KW <= float(tbl_rows[y]["Max_kWr_"])):
    #                    print tbl_rows[y]["Min_kWr_"]
    #                    print tbl_rows[y]["Max_kWr_"]
                        CoolingCOP = tbl_rows[y]["EER"]
                        break
                else:
                    if (Nominal_Total_Capacity_KW > float(tbl_rows[y]["Min_kWr_"])):
    #                    print tbl_rows[y]["Min_kWr_"]
    #                    print tbl_rows[y]["Max_kWr_"]
                        CoolingCOP = tbl_rows[y]["EER"]
                        break
            #print CoolingCOP
            System_Power = Nominal_Total_Capacity_KW / float(CoolingCOP)
            Compressor_Power = System_Power - Cooling_Fan_Power
            System_COP = round(Nominal_Total_Capacity_KW / Compressor_Power,2)

           # print Coil_Name + ' ' + str(Nominal_Total_Capacity_KW) + '\t ' + str(Cooling_Fan_Power) + '\t ' + str(CoolingCOP)

            PTHP_System_objs = ECBC_standard_Idf.idfobjects["HVACTemplate:Zone:PTHP".upper()]
            print Coil_Name
            PTHP_System_obj = [PTHP_Systems for PTHP_Systems in PTHP_System_objs if PTHP_Systems.Zone_Name.upper() == Coil_Name.upper()]
    #        #print VRF_System_obj[0]
            PTHP_System_obj[0].Cooling_Coil_Gross_Rated_COP = System_COP
            PTHP_System_obj[0].Heat_Pump_Heating_Coil_Gross_Rated_COP = System_COP
            #print Coil_Name + '\t ' + str(Nominal_Total_Capacity) + '\t' + str(Nominal_Total_Capacity_KW) + '\t ' + str(Cooling_Fan_Power) + '\t ' + str(CoolingCOP) + '\t ' + str(System_COP)
            print Coil_Name  + "HVACTemplate:Zone:PTHP Modified"
            ECBC_standard_Idf.save()