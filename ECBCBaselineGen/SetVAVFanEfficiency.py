"""Helper function to set system C - Fan motor efficiency   """

from eppy import modeleditor
from eppy.modeleditor import IDF
from eppy.results import readhtml


def getValueFromCSV(P_kW):
    fan_efficiency_csv = "./csv/Fan_efficiency_IS12615_2Pole.csv"
    file = open(fan_efficiency_csv, "r")
    i = 1
    # for every line other than header row
    for line in file:
        if i <= 4:
            # skip header row
            i = i + 1
        else:
            # Read line and split line by ','
            data = line.split(',')
            Rated_output = float(data[1])
            if Rated_output >= P_kW:
                eff_m_IE2 = float(data[10]) / 100
                fan_tot_efficiency = (eff_m_IE2 * 0.6)
                #print "from CSV: " + str(P_kW) + ' ' + str(Rated_output) + ' ' + eff_m_IE2
                break
    return eff_m_IE2, fan_tot_efficiency


def Set_VAV_Fan_Efficiency(Output_HTML, ECBC_standard_Idf):
    # print  "Reading Chiller:Electric:EIR table..."
    filehandle = open(Output_HTML, 'r').read()  # get a file handle to the html file
    htables = readhtml.titletable(filehandle)  # reads the tables with
    tbl = [htable for htable in htables if htable[0] in ["Fans"]]
    a_tbl = tbl[0][1]
    # System_airFlowRate = []
    for rows in range(1, len(a_tbl)):
        VAV_System = a_tbl[rows][0]
        if VAV_System.find('VAV') != -1:
            AirFlowRate = a_tbl[rows][4]
            P_kW = 1.03666 * float(AirFlowRate)
            eff_m_IE2, fan_tot_efficiency = getValueFromCSV(P_kW)
            VAV_System_name = VAV_System.replace(' SUPPLY FAN', '')
            VAV_System_objs = ECBC_standard_Idf.idfobjects["HVACTemplate:System:VAV".upper()]
            VAV_System_obj = [VAV_Systems for VAV_Systems in VAV_System_objs if
                              VAV_Systems.Name.upper() == VAV_System_name.upper()]
            VAV_System_obj[0].Supply_Fan_Total_Efficiency = fan_tot_efficiency
            VAV_System_obj[0].Supply_Fan_Motor_Efficiency = eff_m_IE2
            ECBC_standard_Idf.save()
            print VAV_System_name + ": Fan and Motor efficiency updated"
    return ECBC_standard_Idf


# Html_path = "F:/TestBEP/180218_C/baselineTable.html"
# idf = "F:/TestBEP/180218_C/baseline.idf"
# iddfile = "C:/EnergyPlusV8-6-0/Energy+.idd"
# IDF.setiddname(iddfile)
# ECBC_standard_Idf = IDF(idf)
# Set_VAV_Fan_Efficiency(Html_path, ECBC_standard_Idf)
