from UpdateBuildingEnvelope import copy_objects
from Create_HVAC_Zone_Array import remove_Object_From_CSV

#function to craete a HVAC sizing zone list with coresponding values (Design_Specification_Outdoor_Air_Object_Name, Design_Specification_Zone_Air_Distribution_Object_Name, fan schedule, heating setpoint and cooling setpoint) 
def get_HVAC_Zone_List(ECBC_standard_Idf, System_Zonelist, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    #array to store zone wise information and to store unique schedule name
    HVAC_Zone_List = []
   
    #Read all zonelist   
    for Zonelist in  System_Zonelist:
        #find zonelist name from zone_wise_buildingstory_details array 
        zones_list = [row for row in zone_wise_buildingstory_details if row[0] == Zonelist  and row[3] == "conditioned"]
        
        # find row containg zone detail from ECBC_Zonelist_Wise_Schedule array    
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == Zonelist]
        
        #read all required data
        ECBC_fanSchedule =row[0][7]
        ECBC_heatingSetpoint =row[0][8]
        ECBC_coolingSetpoint =row[0][9]
        for zones_details in zones_list:
            #print zones_details
            zonelist_name = zones_details[0]
            
            zone_name = zones_details[1]
            #print zone_name
            zone_Design_Specification_Outdoor_Air_Object_Name = zones_details[4]
            zone_Design_Specification_Zone_Air_Distribution_Object_Name = zones_details[5]
            
           
            #store value in temp array and append this arry to original array    
            HVAC_Zone_Details = [zone_name, zone_Design_Specification_Outdoor_Air_Object_Name, zone_Design_Specification_Zone_Air_Distribution_Object_Name, ECBC_fanSchedule, ECBC_heatingSetpoint, ECBC_coolingSetpoint]
            HVAC_Zone_List.append(HVAC_Zone_Details)
       
    #print HVAC_Zone_List
    return HVAC_Zone_List

#function to add sizing parameter object
def add_Sizing_Parameter(ECBC_standard_Idf, ECBC_SysD_Library, heating_Sizing_Factor, cooling_Sizing_Factor):
    print "Creating Sizing:Parameter object...."
    #copy sizing parameter object from standard idf to ECBC_standard_Idf and set its attribute value and save
    sizing_parameter_objs = ECBC_SysD_Library.idfobjects["Sizing:Parameters".upper()][0]
    sizing_parameter_objs.Heating_Sizing_Factor=heating_Sizing_Factor
    sizing_parameter_objs.Cooling_Sizing_Factor=cooling_Sizing_Factor
    sizing_parameter_objs.Timesteps_in_Averaging_Window=""
    ECBC_standard_Idf.copyidfobject(sizing_parameter_objs)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf

#function to add one HVAC Thermostat object 
def add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_SysD_Library, zone_name, ECBC_heatingSetpoint, ECBC_coolingSetpoint):
    print "Creating " + zone_name+"_Thermostat HVACTemplate:Thermostat object...."
    #create new HVAC thermostat object from standard idf
    new_thermostat_obj = ECBC_SysD_Library.idfobjects["HVACTemplate:Thermostat".upper()][0]   
    #set schedule from csv
    #set attribute value of objects
    Thermostat_Name  = zone_name+"_Thermostat"
    new_thermostat_obj.Name=Thermostat_Name
    new_thermostat_obj.Heating_Setpoint_Schedule_Name = ECBC_heatingSetpoint
    new_thermostat_obj.Cooling_Setpoint_Schedule_Name = ECBC_coolingSetpoint
    new_thermostat_obj.Constant_Heating_Setpoint = ""
    new_thermostat_obj.Constant_Cooling_Setpoint = ""
    #copy that object to ECBC_standard_Idf
    ECBC_standard_Idf.copyidfobject(new_thermostat_obj)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf

#function to add one HVAC zone SysD object 
def add_HVAC_Zone_SysD(ECBC_standard_Idf, ECBC_SysD_Library, zone_name, design_Specification_Outdoor_Air_Object_Name, design_Specification_Zone_Air_Distribution_Object_Name):
    
    print "Creating " + zone_name  + " HVACTemplate:Zone:Unitary..."
    #read HVAC zone SysD object from standard idf
    Thermostat_Name  = zone_name+"_Thermostat"
    new_SysD_Zone_obj = ECBC_SysD_Library.idfobjects["HVACTemplate:Zone:Unitary".upper()][0]   
    new_SysD_Zone_obj.Zone_Name = zone_name       
    new_SysD_Zone_obj.Template_Unitary_System_Name = zone_name + "_System"
    new_SysD_Zone_obj.Template_Thermostat_Name = Thermostat_Name
    new_SysD_Zone_obj.Design_Specification_Outdoor_Air_Object_Name = design_Specification_Outdoor_Air_Object_Name
    new_SysD_Zone_obj.Design_Specification_Zone_Air_Distribution_Object_Name =  design_Specification_Zone_Air_Distribution_Object_Name
    #new_SysD_Zone_obj.Supply_Fan_Operating_Mode_Schedule_Name = ECBC_fanSchedule
    #new_SysD_Zone_obj.Zone_Cooling_Design_Supply_Air_Temperature = float(cooling_SetPoint) - 11
    
    ECBC_standard_Idf.copyidfobject(new_SysD_Zone_obj)
    ECBC_standard_Idf.save()
    # replace fan / Hvac schedule
    return ECBC_standard_Idf


#function to add HVAC System SysD from list passed as parameter    
def add_HVAC_System_SysD(ECBC_standard_Idf,ECBC_SysD_Library, SysD_System_Name,ECBC_fanSchedule, cooling_SetPoint):
    
    print "Creating " + SysD_System_Name  + "_System HVACTemplate:System:UnitarySystem..."
    new_SysD_System_obj = ECBC_SysD_Library.idfobjects["HVACTemplate:System:UnitarySystem".upper()][0]
    #SysD_System_Name = SysD_System_Name 
    new_SysD_System_obj.Name = SysD_System_Name + "_System"
    new_SysD_System_obj.System_Availability_Schedule_Name = ECBC_fanSchedule
    new_SysD_System_obj.Supply_Fan_Operating_Mode_Schedule_Name = ECBC_fanSchedule
    new_SysD_System_obj.Control_Zone_or_Thermostat_Location_Name = SysD_System_Name
    new_SysD_System_obj.Cooling_Design_Supply_Air_Temperature=cooling_SetPoint - 11

    ECBC_standard_Idf.copyidfobject(new_SysD_System_obj)       
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf 


def creating_HVAC_SystemType_D(ECBC_standard_Idf,System_Zonelist, ECBC_SysD_Library, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details, cooling_SetPoint):
    
    print "get HVAC Zone List"
    #function call to fet zone wise details and unique schedule name
    HVAC_Zone_List = get_HVAC_Zone_List(ECBC_standard_Idf,System_Zonelist, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    #ECBC_standard_Idf = remove_Object_From_CSV(ECBC_standard_Idf)
    #ECBC_standard_Idf = add_Sizing_Parameter(ECBC_standard_Idf, ECBC_SysD_Library, heating_Sizing_Factor, cooling_Sizing_Factor)

    
    for zone_details in HVAC_Zone_List:
         #zone_name , heeting setpoint, cooling setpoint
        ECBC_standard_Idf = add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_SysD_Library, zone_details[0],zone_details[4],zone_details[5])
        print "Creating HVACTemplate:Zone:Unitary object...."
        ECBC_standard_Idf = add_HVAC_Zone_SysD(ECBC_standard_Idf, ECBC_SysD_Library, zone_details[0], zone_details[1], zone_details[2])
        ECBC_standard_Idf = add_HVAC_System_SysD(ECBC_standard_Idf,ECBC_SysD_Library, zone_details[0],zone_details[3], cooling_SetPoint)
        
       
    return ECBC_standard_Idf