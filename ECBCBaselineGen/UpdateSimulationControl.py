"""helper function to update Simulation Control object""" 
from Create_HVAC_Zone_Array import remove_Object_From_CSV
#function to update SimulationControl object
def update_SimulationControl(ECBC_standard_Idf):
    print "Updating Simulation Control Object..."
    #read Simulation control object and update it's attribute value
    simulationcontrol_obj = ECBC_standard_Idf.idfobjects["SimulationControl".upper()][0]
    simulationcontrol_obj.Do_Zone_Sizing_Calculation = "Yes"
    simulationcontrol_obj.Do_System_Sizing_Calculation = "Yes"
    simulationcontrol_obj.Do_Plant_Sizing_Calculation = "Yes"
    simulationcontrol_obj.Run_Simulation_for_Sizing_Periods = "No"
    simulationcontrol_obj.Run_Simulation_for_Weather_File_Run_Periods = "Yes"
    simulationcontrol_obj.Do_HVAC_Sizing_Simulation_for_Sizing_Periods = "Yes"
    #save ECBC_standard_Idf
    ECBC_standard_Idf.save()
    print "Simulation Control Object Updated."
    return ECBC_standard_Idf

#function to update SizingPeriodDesignDay_TimeStep object
def update_SizingPeriodDesignDay_TimeStep(ECBC_standard_Idf):
    print "Updating Time Step Object...."
    #reading Timesteps object and updating its attribute value
    timestep_obj = ECBC_standard_Idf.idfobjects["timestep".upper()][0]
    timestep_obj.Number_of_Timesteps_per_Hour = 1
    #save ECBC_standard_Idf
    ECBC_standard_Idf.save()
    print "Time Step Object Updated."
    return ECBC_standard_Idf

#function to remove all objects passed as parameter
def removeObjects(ECBC_standard_Idf,object_name):
    #read all objects
    obj_list = ECBC_standard_Idf.idfobjects[object_name.upper()]
    #get number of object
    count_obj =  len(obj_list)
    #for each object
    for x in range(0,count_obj):
        #pop one element from starting
        ECBC_standard_Idf.popidfobject(object_name.upper(),0)
    ECBC_standard_Idf.save()
    print "All " + object_name + " Object Removed."    
    return ECBC_standard_Idf;
    
#function to modify  SizingPeriod 
def set_SizingPeriod(ECBC_standard_Idf):   
    #remove all SizingPeriod:DesignDay and SizingPeriod:WeatherFileConditionType object
    removeObjects(ECBC_standard_Idf, "SizingPeriod:DesignDay")
    removeObjects(ECBC_standard_Idf, "SizingPeriod:WeatherFileConditionType")
    #add 2 new SizingPeriod:WeatherFileDays objects - 'Summer Design Day' and 'Winter Design Day'
    print "Adding SizingPeriod:WeatherFileDays Object...."
    new_sizingperiod_obj = ECBC_standard_Idf.newidfobject("SizingPeriod:WeatherFileDays".upper())
    new_sizingperiod_obj.Name = "Summer Design Day"
    new_sizingperiod_obj.Begin_Month = 1
    new_sizingperiod_obj. Begin_Day_of_Month = 1
    new_sizingperiod_obj.End_Month = 12
    new_sizingperiod_obj.End_Day_of_Month = 31
    new_sizingperiod_obj.Day_of_Week_for_Start_Day = "Monday"
    new_sizingperiod_obj.Use_Weather_File_Daylight_Saving_Period = "Yes"
    new_sizingperiod_obj.Use_Weather_File_Rain_and_Snow_Indicators = "Yes"
    ECBC_standard_Idf.save()
    print "Summer Design Day Added."
    new_sizingperiod_obj = ECBC_standard_Idf.newidfobject("SizingPeriod:WeatherFileDays".upper())
    new_sizingperiod_obj.Name = "Winter Design Day"
    new_sizingperiod_obj.Begin_Month = 1
    new_sizingperiod_obj. Begin_Day_of_Month = 1
    new_sizingperiod_obj.End_Month = 12
    new_sizingperiod_obj.End_Day_of_Month = 31
    new_sizingperiod_obj.Day_of_Week_for_Start_Day = "Monday"
    new_sizingperiod_obj.Use_Weather_File_Daylight_Saving_Period = "Yes"
    new_sizingperiod_obj.Use_Weather_File_Rain_and_Snow_Indicators = "Yes"
    ECBC_standard_Idf.save()
    print "Winter Design Day Added."
    return ECBC_standard_Idf 

#function to set Run Period object
def update_RunPeriod(ECBC_standard_Idf):
    print "Updating RunPeriod Object...."
    #read RunPeriod object and update it's attribute value
    runPeriod_obj=ECBC_standard_Idf.idfobjects["RunPeriod".upper()][0]
    runPeriod_obj.Name="Annual"
    runPeriod_obj.Begin_Month=1
    runPeriod_obj.Begin_Day_of_Month=1
    runPeriod_obj.End_Month=12
    runPeriod_obj.End_Day_of_Month=31
    runPeriod_obj.Day_of_Week_for_Start_Day="UseWeatherFile"
    runPeriod_obj.Use_Weather_File_Holidays_and_Special_Days="No"
    runPeriod_obj.Use_Weather_File_Daylight_Saving_Period="Yes"
    runPeriod_obj.Apply_Weekend_Holiday_Rule="Yes"
    runPeriod_obj.Use_Weather_File_Rain_Indicators="Yes"
    runPeriod_obj.Use_Weather_File_Snow_Indicators="Yes"
    runPeriod_obj.Number_of_Times_Runperiod_to_be_Repeated=1
    ECBC_standard_Idf.save()
    print "Run Period updated"
    return ECBC_standard_Idf

#function to add sizing parameter object
def add_Sizing_Parameter(ECBC_standard_Idf, heating_Sizing_Factor, cooling_Sizing_Factor):
    print "Creating Sizing:Parameter object...."
    #copy sizing parameter object from standard idf to ECBC_standard_Idf and set its attribute value and save
    sizing_parameter_objs = ECBC_standard_Idf.newidfobject("Sizing:Parameters".upper())
    sizing_parameter_objs.Heating_Sizing_Factor=heating_Sizing_Factor
    sizing_parameter_objs.Cooling_Sizing_Factor=cooling_Sizing_Factor
    sizing_parameter_objs.Timesteps_in_Averaging_Window=""
    
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf

#main Function to call all method
def fn_simulationControl(ECBC_standard_Idf, heating_Sizing_Factor, cooling_Sizing_Factor):
    ECBC_standard_Idf = update_SimulationControl(ECBC_standard_Idf)
    ECBC_standard_Idf = update_SizingPeriodDesignDay_TimeStep(ECBC_standard_Idf)
    ECBC_standard_Idf = set_SizingPeriod(ECBC_standard_Idf)
    ECBC_standard_Idf = update_RunPeriod(ECBC_standard_Idf)
    ECBC_standard_Idf = remove_Object_From_CSV(ECBC_standard_Idf)
    ECBC_standard_Idf = add_Sizing_Parameter(ECBC_standard_Idf, heating_Sizing_Factor, cooling_Sizing_Factor)
    return ECBC_standard_Idf