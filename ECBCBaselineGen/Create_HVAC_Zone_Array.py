from Generate_List_From_CSV import get_ECBC_Object_Remove_list

#function to remove all objects passed as parameter
def removeObject(ECBC_standard_Idf,object_name):
    print "Removing all " + object_name + "  objects..."   
    #read all object by passing name
    obj_list = ECBC_standard_Idf.idfobjects[object_name.upper()]
    #for each object
    length =  len(obj_list)
    for x in range(0,length):
        #remove all object
        ECBC_standard_Idf.popidfobject(object_name.upper(),0)
    ECBC_standard_Idf.save()
     
    return ECBC_standard_Idf;



def remove_Object_From_CSV(ECBC_standard_Idf):
 
    ECBC_Object_Remove_list = get_ECBC_Object_Remove_list()
    #print ECBC_Object_Remove_list
        
    for ECBC_Object in ECBC_Object_Remove_list:
        ECBC_standard_Idf = removeObject(ECBC_standard_Idf,ECBC_Object)

    return ECBC_standard_Idf
        