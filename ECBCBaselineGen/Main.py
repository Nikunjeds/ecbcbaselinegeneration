##This is designed in python 2.7
## To run this use command 'C:\python27\python.exe main.py'


# import the library
from appJar import gui
from os.path import basename
from os.path import splitext
import time
from Cr_Baseline import Generate_Baseline

# from fbButton import method

# handle button events
def press(button):
    if button == "Cancel":
        app.stop()
    else:
        wall_uvalue = app.getEntry("Wall U-Value")
        roof_uvalue = app.getEntry("Roof U-Value")
        window_shgc_n = app.getEntry("Window SHGC North")
        window_shgc_nn = app.getEntry("Window SHGC Non-North")
        cooling_setpt = app.getEntry("Cooling Set Point")
        heating_sf = app.getEntry("Heating Sizing factor")
        cooling_sf = app.getEntry("Cooling Sizing factor")
        bu_area = app.getEntry("Built-up Area")
        heat_fuel_src = app.getOptionBox("Heat Fuel Source")
        climate = app.getOptionBox("Climate")
        climate = climate[climate.find(':')+ 1:]
        climate = climate.strip()
        occ = app.getOptionBox("Occupancy")
        typology = app.getOptionBox("Typology")
        typology = typology[typology.find(':') + 1:]
        typology = typology.strip()
        bu_class = app.getOptionBox("Building-Class")
        bu_class = bu_class[bu_class.find(':') + 1:]
        bu_class = bu_class.strip()
        bu_class = typology + "-" + bu_class
        lat = app.getEntry("Latitude")
        compliance = app.getOptionBox("Compliance")
        IDF_P = app.getEntry("proposed")
        WeatherFileName = app.getEntry("weatherFile")
        WeatherFileName = basename(WeatherFileName)
        WeatherFileName = splitext(WeatherFileName)[0]
        #WeatherFileName = app.getOptionBox("weatherFile")
        # method(usr, pwd)
        print str(wall_uvalue) + " " + str(roof_uvalue) + " " + str(window_shgc_n) + " " + str(window_shgc_nn) + " " + str(cooling_setpt) + " " + str(heating_sf) + " " + str(cooling_sf) + " " + str(bu_area) + " " + heat_fuel_src + " " + climate + " " + occ + " " + typology + " " + bu_class + " " + str(lat) + " " + compliance
        print IDF_P
        print(WeatherFileName)
        start_time = time.time()

        # Note: pass only name of weather file without extension and this should be in energyplus weather folder

        Generate_Baseline(IDF_P, wall_uvalue, roof_uvalue, window_shgc_n, window_shgc_nn, cooling_setpt, heating_sf, cooling_sf, heat_fuel_src, WeatherFileName, bu_area, climate, occ, typology, bu_class, lat, compliance)
        app.stop()

        print("--- %s seconds ---" % (time.time() - start_time))

# create a GUI variable called app
app = gui("Login Window", "500x600")
# app.setBg("orange")
app.startLabelFrame("Baseline Automation")
# these only affect the labelFrame
app.setSticky("ew")
app.setFont(10)



# Climate = "WH"
# Occupancy = "Daytime"
# Typology = "Healthcare"
# BuildingClass = "Ed-Insti"  #"Ed-Sch" #"
# lat = 20
# compliance = 'ECBC'

app.addLabel("l1", "Wall U-Value", 0, 0)
app.addNumericEntry("Wall U-Value", 0, 1)
app.setEntry("Wall U-Value", "0.40")

app.addLabel("l2", "Roof U-Value", 1, 0)
app.addNumericEntry("Roof U-Value", 1, 1)
app.setEntry("Roof U-Value", "0.33")

app.addLabel("l3", "Window SHGC North", 2, 0)
app.addNumericEntry("Window SHGC North", 2, 1)
app.setEntry("Window SHGC North", "0.50")

app.addLabel("l4", "Window SHGC Non-North", 3, 0)
app.addNumericEntry("Window SHGC Non-North", 3, 1)
app.setEntry("Window SHGC Non-North", "0.27")

app.addLabel("l5", "Cooling Set Point", 4, 0)
app.addNumericEntry("Cooling Set Point", 4, 1)
app.setEntry("Cooling Set Point", "25")

app.addLabel("l6", "Heating Sizing factor", 5, 0)
app.addNumericEntry("Heating Sizing factor", 5, 1)
app.setEntry("Heating Sizing factor", "1.25")

app.addLabel("l7", "Cooling Sizing factor", 6, 0)
app.addNumericEntry("Cooling Sizing factor", 6,1)
app.setEntry("Cooling Sizing factor", "1.75")

app.addLabel("l8", "Built-up Area", 7, 0)
app.addNumericEntry("Built-up Area", 7, 1)
app.setEntry("Built-up Area", "20000")

app.addLabel("l9", "Heat Fuel Source", 8, 0)
app.addOptionBox("Heat Fuel Source", ["Elect", "No Heating", "Fossil"], 8,1)

app.addLabel("l10", "Climate", 9, 0)
app.addOptionBox("Climate",
                      ["Cold : Cold", "Composite : Com", "Warm & Humid : WH", "Hot & Dry :HD ", "Temprate :Te"], 9, 1)

app.addLabel("l11", "Occupancy", 10, 0)
app.addOptionBox("Occupancy", ["Daytime", "24Hr"], 10, 1)

app.addLabel("l12", "Typology", 11, 0)
app.addOptionBox("Typology",
                      ["Hospitality : Ho", "Healthcare : HC", "Educational : Ed", "Shopping Complex : Sc", "Business : Bu", "Assembly : As"], 11, 1)

app.addLabel("l13", "Building-Class", 12, 0)
app.addOptionBox("Building-Class",
                      ["Star Hotel : *Ho", "No Star Hotel : Ho", "Resort : Res", "College : Col", "University : Uni",
                       "Institution : Insti", "School : Sch", "Hospital : Hos", "Outpatient : OP", "Mall : Mall",
                       "Standalone Retail : SO Re", "Open Gallery Mall : OGM", "Supermarket : SM",
                       "Large Office (> 30k sqm) : Off(>30k)", "Medium Office (10k to 30k sqm) : Off(10-30k)",
                       "Small Office (< 10k sqm) : Off(<10k)", "Multiplex : Mx", "Theatre : Thea",
                       "Building for transport : Trans"], 12, 1)

app.addLabel("l14", "Latitude", 13, 0)
app.addNumericEntry("Latitude", 13, 1)
app.setEntry("Latitude", "10")

app.addLabel("l15", "Compliance", 14, 0)
app.addOptionBox("Compliance", ["ECBC", "ECBC+", "Super ECBC"], 14, 1)

app.addLabel("l16", "Proposed IDF", 15, 0)
app.addFileEntry("proposed", 15, 1)
app.setEntry("Window SHGC North", "0.50")

app.addLabel("l17", "Weather File", 16, 0)
app.addFileEntry("weatherFile", 16, 1)
# app.addOptionBox("weatherFile",
#                       ["IND_DL_New.Delhi-Safdarjung.AP.421820_ISHRAE2014",
#                        "IND_Ahmedabad.426470_ISHRAE",
#                        "USA_CA_San.Francisco.Intl.AP.724940_TMY3",
#                        "USA_CO_Golden-NREL.724666_TMY3",
#                        "USA_FL_Tampa.Intl.AP.722110_TMY3"], 16, 1)

# link the buttons to the function called press
app.addButtons(["Submit", "Cancel"], press, 17, 0, 17)
app.stopLabelFrame()
app.setFocus("Wall U-Value")

# start the GUI
app.go()
