from UpdateSchedule import update_schedule
from UpdateOutputControlReportingTolerances import update_outputControl_ReportingTolerance
from UpdateSimulationControl import update_SizingPeriodDesignDay_TimeStep
from UpdateSimulationControl import update_RunPeriod


def modify_Proposed(Proposed_IDF, ECBC_Schedule_Library, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    print "Modifying Proposed IDF... \n"
    print "Updating TimeStep object in Proposed IDF..."
    Proposed_IDF = update_SizingPeriodDesignDay_TimeStep(Proposed_IDF)
    print "Updating RunPeriod object in Proposed IDF..."
    Proposed_IDF = update_RunPeriod(Proposed_IDF)
    print "Updating outputControl_ReportingTolerance object in Proposed IDF..."
    Proposed_IDF = update_outputControl_ReportingTolerance(Proposed_IDF)
    print "Updating Shedules in Proposed IDF..."
    Proposed_IDF = update_schedule(ECBC_Schedule_Library,Proposed_IDF, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    Proposed_IDF.save()
    return Proposed_IDF