"""helper function for checking WWR <= 40, if not Set WWR by resizing all fenestration object""" 

# function to get WWR by checking all Exterior wall surface and winow surface
def get_WWR_SRR(ECBC_standard_Idf, wall_list, roof_list):
    # repeated logic in envelope 
    total_wall_area = 0
    total_window_area = 0
    total_roof_area = 0
    total_skylight_area = 0
    
    #Read all Surfaces
    surface_objs = ECBC_standard_Idf.idfobjects["BuildingSurface:Detailed".upper()]
    #For Each Surface in Surface list
    for surface in surface_objs:
        #get Surface details - Name, Outside_Boundary_Condition and Surface_type
        surface_nm = surface.Name
        surface_outside_boundary = surface.Outside_Boundary_Condition
        surface_type = surface.Surface_Type
        #Check for all surface where Outside_Boundary_Condition is Outdoor and Surface_type is wall or roof
        #get all Exterior wall surface area and total exterior roof area
        if (surface_outside_boundary == "Outdoors"):
            if (surface_type == "Wall"):
                #Add area of surface to get total Outer Surface Area
                total_wall_area = total_wall_area + surface.area
            if (surface_type == "Roof"):
                #Add area of surface to get total Outer Surface Area
                total_roof_area = total_roof_area + surface.area
    #Read All Window Surface
    fenestration_objs = ECBC_standard_Idf.idfobjects["FenestrationSurface:Detailed".upper()]
    #for each Window get its area and add to get total window area and total skylight area
    for fenestration in fenestration_objs:
        if fenestration.Building_Surface_Name in wall_list:
            total_window_area = total_window_area + fenestration.area
        if fenestration.Building_Surface_Name in roof_list:
            total_skylight_area = total_skylight_area + fenestration.area
    
    #Calculate WWR
    wwr = (total_window_area / total_wall_area) * 100
    print "Total surface area = %s" % (total_wall_area, )
    print "Total window area = %s" % (total_window_area, )
    print "WWR = %s" % (wwr, )
    
    #Calculate WWR
    srr = (total_skylight_area / total_roof_area) * 100
    print "Total roof area = %s" % (total_roof_area, )
    print "Total skylight area = %s" % (total_skylight_area, )
    print "SRR = %s" % (srr, )
    #return to function 
    return wwr, srr

#not used
def setWindowZ_Method1(ECBC_standard_Idf,factor_i):
    rule_objs = ECBC_standard_Idf.idfobjects["GlobalGeometryRules".upper()][0]
    vertex_position = rule_objs.Starting_Vertex_Position
    vertex_direction = rule_objs.Vertex_Entry_Direction
    #calling getRuleCondition to get condition number on the basis of  vertex_position,vertex_direction
    geometry_condition = getRuleCondition(vertex_position,vertex_direction)
    if geometry_condition == 1:  
        fenestration_objs = ECBC_standard_Idf.idfobjects["FenestrationSurface:Detailed".upper()]
        for fenestration in fenestration_objs:               
            window_name = fenestration.Name     
            v1_z = fenestration.Vertex_1_Zcoordinate
            v2_z = fenestration.Vertex_2_Zcoordinate
            h = abs(v1_z - v2_z)
            factor_j = h * factor_i
            status_msg = window_name + " coordinates modiefied from " + str(fenestration.Vertex_1_Zcoordinate) + " " + str(fenestration.Vertex_4_Zcoordinate)
            fenestration.Vertex_1_Zcoordinate= fenestration.Vertex_2_Zcoordinate + factor_j
            fenestration.Vertex_4_Zcoordinate= fenestration.Vertex_3_Zcoordinate + factor_j

            status_msg += " to " + str(round(fenestration.Vertex_1_Zcoordinate,2)) + " " + str(round(fenestration.Vertex_4_Zcoordinate,2))
            print status_msg
    else:
        print "Geometry condition not match"

   # wwr = get_WWR_SRR(ECBC_standard_Idf)   
    ECBC_standard_Idf.save()

    # not used 
def setWindowZ_Method2(ECBC_standard_Idf,factor_i):
    fenestration_objs = ECBC_standard_Idf.idfobjects["FenestrationSurface:Detailed".upper()]
    for fenestration in fenestration_objs:
        window_name = fenestration.Name

        v1_z = fenestration.Vertex_1_Zcoordinate
        v2_z = fenestration.Vertex_2_Zcoordinate
        v4_z = fenestration.Vertex_4_Zcoordinate
        h1 = abs(v1_z - v2_z)
        h2 = abs(v1_z - v4_z)
        #print d1
        #print d2
        if(h1 > 0 ):
            factor_j = h1 * factor_i
            if v1_z > v2_z:
                fenestration.Vertex_2_Zcoordinate= fenestration.Vertex_1_Zcoordinate - factor_j
                fenestration.Vertex_3_Zcoordinate= fenestration.Vertex_4_Zcoordinate - factor_j
            else:
                fenestration.Vertex_1_Zcoordinate= fenestration.Vertex_2_Zcoordinate - factor_j
                fenestration.Vertex_4_Zcoordinate= fenestration.Vertex_3_Zcoordinate - factor_j
        elif (h2 > 0):
            factor_j = h2 * factor_i
            if v1_z > v4_z:
                fenestration.Vertex_4_Zcoordinate= fenestration.Vertex_1_Zcoordinate - factor_j
                fenestration.Vertex_3_Zcoordinate= fenestration.Vertex_2_Zcoordinate - factor_j
            else:
                fenestration.Vertex_1_Zcoordinate= fenestration.Vertex_4_Zcoordinate - factor_j
                fenestration.Vertex_2_Zcoordinate= fenestration.Vertex_3_Zcoordinate - factor_j
        else:
            print "c3"          
   #
    ECBC_standard_Idf.save()
    
def setWindowZ_Method3(fenestration,factor_i):
    
    window_name = fenestration.Name

    v1_z = fenestration.Vertex_1_Zcoordinate
    v2_z = fenestration.Vertex_2_Zcoordinate
    v4_z = fenestration.Vertex_4_Zcoordinate
    h1 = abs(v1_z - v2_z)
    h2 = abs(v1_z - v4_z)
#    print h1
#    print h2
    if(h1 > 0 ):
        factor_j = h1 * factor_i
        if v1_z > v2_z:
            fenestration.Vertex_2_Zcoordinate= fenestration.Vertex_1_Zcoordinate - factor_j
            fenestration.Vertex_3_Zcoordinate= fenestration.Vertex_4_Zcoordinate - factor_j
        else:
            fenestration.Vertex_1_Zcoordinate= fenestration.Vertex_2_Zcoordinate - factor_j
            fenestration.Vertex_4_Zcoordinate= fenestration.Vertex_3_Zcoordinate - factor_j
    elif (h2 > 0):
        factor_j = h2 * factor_i
        if v1_z > v4_z:
            fenestration.Vertex_4_Zcoordinate= fenestration.Vertex_1_Zcoordinate - factor_j
            fenestration.Vertex_3_Zcoordinate= fenestration.Vertex_2_Zcoordinate - factor_j
        else:
            fenestration.Vertex_1_Zcoordinate= fenestration.Vertex_4_Zcoordinate - factor_j
            fenestration.Vertex_2_Zcoordinate= fenestration.Vertex_3_Zcoordinate - factor_j
    else:
        print "NA"          
   #
    return fenestration
    
def setSkylightX_Method3(fenestration,factor_i):
    
    window_name = fenestration.Name

    v1_x = fenestration.Vertex_1_Xcoordinate
    v2_x = fenestration.Vertex_2_Xcoordinate
    v4_x = fenestration.Vertex_4_Xcoordinate
    h1 = abs(v1_x - v2_x)
    h2 = abs(v1_x - v4_x)
#    print h1
#    print h2
    if(h1 > 0 ):
        factor_j = h1 * factor_i
        if v1_x > v2_x:
            fenestration.Vertex_2_Xcoordinate= fenestration.Vertex_1_Xcoordinate - factor_j
            fenestration.Vertex_3_Xcoordinate= fenestration.Vertex_4_Xcoordinate - factor_j
        else:
            fenestration.Vertex_1_Xcoordinate= fenestration.Vertex_2_Xcoordinate - factor_j
            fenestration.Vertex_4_Xcoordinate= fenestration.Vertex_3_Xcoordinate - factor_j
    elif (h2 > 0):
        factor_j = h2 * factor_i
        if v1_x > v4_x:
            fenestration.Vertex_4_Xcoordinate= fenestration.Vertex_1_Xcoordinate - factor_j
            fenestration.Vertex_3_Xcoordinate= fenestration.Vertex_2_Xcoordinate - factor_j
        else:
            fenestration.Vertex_1_Xcoordinate= fenestration.Vertex_4_Xcoordinate - factor_j
            fenestration.Vertex_2_Xcoordinate= fenestration.Vertex_3_Xcoordinate - factor_j
    else:
        print "NA"          
   #
    return fenestration
    
       

#function to set z axis of Vertex to manage WWR 40%
def manage_WWR_SRR(ECBC_standard_Idf, wall_list, roof_list):
    #function call to getWWR
    wwr, srr = get_WWR_SRR(ECBC_standard_Idf, wall_list, roof_list)
    wwr_threeshold = 40
    srr_threeshold = 5
    wwr_status = 'n'
    srr_status = 'n'
    msg = ""
    if(wwr < wwr_threeshold):    
        msg += "WWR is less than " + str(wwr_threeshold) + " , so no resizing is required\n"
    else:
        factor_i = wwr_threeshold / wwr
        wwr_status = 'y'
     
    if(srr < srr_threeshold):    
        msg += "SRR is less than " + str(srr_threeshold) + ", so no resizing is required\n"
    else:
        factor_i_skylight = srr_threeshold / srr
        srr_status = 'y'
    
    #print srr_status
    if (wwr > wwr_threeshold or srr > srr_threeshold):
        print "Setting WWR and SRR..."
        fenestration_objs = ECBC_standard_Idf.idfobjects["FenestrationSurface:Detailed".upper()]
        for fenestration in fenestration_objs:
            if (fenestration.Building_Surface_Name in wall_list and wwr_status =='y'): 
                
                fenestration = setWindowZ_Method3(fenestration,factor_i)
                ECBC_standard_Idf.save()
            elif (fenestration.Building_Surface_Name in roof_list and srr_status =='y'):
                fenestration = setSkylightX_Method3(fenestration,factor_i_skylight)
                ECBC_standard_Idf.save()
                
    
                  
    wwr, srr = get_WWR_SRR(ECBC_standard_Idf, wall_list, roof_list)    
    msg += "WWR and SRR Check Completed\n"
    return msg
        
    
# function to set 1,2,3.... condition number on the basis of  vertex_position,vertex_direction   
# 1 = vertex_position = "UpperLeftCorner" and vertex_direction = "Counterclockwise"
def getRuleCondition(vertex_position,vertex_direction)  :
    if (vertex_position == "UpperLeftCorner" and vertex_direction == "Counterclockwise"):
        return 1
    else:
        return 0