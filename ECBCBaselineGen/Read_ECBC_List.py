"""helper function to get ECBC Schedule and zonelist from csv"""

def get_ECBC_Zonelist():
    ECBC_Zonelist_csv = "./csv/ECBC_Zonelist.csv"
    ECBC_Zonelist = []
    ECBC_ZoneListWiseSchedule = []
    file = open(ECBC_Zonelist_csv,"r")
    i = 1
    for line in file: 
        if i == 1 :
            #skip header row
            i= i+ 1
        else:    
            data = line.split(',')
            ECBC_Zonelist.append(data[0].strip())
            arr = []
            for val in data:
                arr.append(val.strip())  #= [data[0].strip(),data[1].strip(),data[2].strip(),data[3].strip(),data[3].strip()]
           # schedule_arr.append(arr)       
            ECBC_ZoneListWiseSchedule.append(arr)
            
    
    #print ECBC_Zonelist  
    #print ECBC_ZoneListWiseSchedule
    return ECBC_Zonelist,ECBC_ZoneListWiseSchedule

 
def get_ECBC_Object_Remove_list(ECBC_standard_Idf):
    ECBC_Schedule_Remove_csv = "./csv/ECBC_Schedule_Remove.csv"
    ECBC_Schedule_Remove_list = []
    file = open(ECBC_Schedule_Remove_csv,"r")
    for line in file:         
        data = line.split(',')
        ECBC_Schedule_Remove_list.append(data[0].strip())
       
    #print ECBC_Zonelist  
    #print ECBC_ZoneListWiseSchedule
    return ECBC_Schedule_Remove_list

#get_ECBC_Zonelist()        