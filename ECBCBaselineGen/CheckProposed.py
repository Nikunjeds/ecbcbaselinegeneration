from ExtractTableData.Constants import get_constants
from eppy import modeleditor
from eppy.modeleditor import IDF

def CoolingTowerFan_Validation(Proposed_Idf, BuiltUpArea):
    if BuiltUpArea > 20000:
        CoolingTower_SingleSpeed_objs = Proposed_Idf.idfobjects["CoolingTower:SingleSpeed".upper()]
        CoolingTower_TwoSpeed_objs = Proposed_Idf.idfobjects["CoolingTower:TwoSpeed".upper()]
        CoolingTower_VariableSpeed_Merkel_objs = Proposed_Idf.idfobjects["CoolingTower:VariableSpeed:Merkel".upper()]
        CoolingTower_VariableSpeed_objs = Proposed_Idf.idfobjects["CoolingTower:VariableSpeed".upper()]
        if len(CoolingTower_SingleSpeed_objs) > 0:
            status_msg = "Error 3: Invalid IDF!! Cooling Tower Fan Validation fails\n"
            return status_msg
        else:
            if len(CoolingTower_TwoSpeed_objs) > 0:
                for CoolingTower_TwoSpeed_obj in CoolingTower_TwoSpeed_objs:
                    Hign_Fan_Speed = CoolingTower_TwoSpeed_obj.High_Fan_Speed_Fan_Power
                    Low_Fan_Speed = CoolingTower_TwoSpeed_obj.Low_Fan_Speed_Fan_Power
                    FanPowerRatio = Low_Fan_Speed / Hign_Fan_Speed
                    if FanPowerRatio > 0.67:
                        status_msg = "Error 3: Invalid IDF!! Cooling Tower Fan Validation fails\n"
                        return status_msg
                    # break

    return "CoolingTowerFan Validation Completed Successfully!!\n"


def Eval_Proposed_System_Efficiency(Proposed_Idf, compliance):
    # Evaluate System A Efficiency
    Equiment_name_arr = []
    autosize_arr = []
    CoolingCoil_SingleSpeed_objs = Proposed_Idf.idfobjects["Coil:Cooling:DX:SingleSpeed".upper()]
    for CoolingCoil_SingleSpeed_obj in CoolingCoil_SingleSpeed_objs:
        tot_Cooling_Cap = CoolingCoil_SingleSpeed_obj.Gross_Rated_Total_Cooling_Capacity
        if tot_Cooling_Cap == "Autosize" or tot_Cooling_Cap == "autosize":
            autosize_arr.append(CoolingCoil_SingleSpeed_obj.Name)
        else:
            tot_Cooling_Cap_KW = tot_Cooling_Cap / 1000
            cooling_COP = CoolingCoil_SingleSpeed_obj.Gross_Rated_Cooling_COP
            condenser_Type = CoolingCoil_SingleSpeed_obj.Condenser_Type
            proposed_COP = COP_AfterincludingFanPower(tot_Cooling_Cap_KW, cooling_COP)
            ECBC_COP = get_ECBC_COP('USPAC_A', tot_Cooling_Cap_KW, "EER", compliance)
            if proposed_COP >= ECBC_COP:
                print CoolingCoil_SingleSpeed_obj.Name + " Meet"
            else:
                Equiment_name_arr.append([CoolingCoil_SingleSpeed_obj.Name,proposed_COP, ECBC_COP])

    # Evaluate System B Efficiency
    AC_VariableRefFlow_objs = Proposed_Idf.idfobjects["AirConditioner:VariableRefrigerantFlow".upper()]
    for AC_VariableRefFlow_obj in AC_VariableRefFlow_objs:
        cooling_Cap = AC_VariableRefFlow_obj.Gross_Rated_Total_Cooling_Capacity
        cooling_COP = AC_VariableRefFlow_obj.Gross_Rated_Cooling_COP
        heating_Cap = AC_VariableRefFlow_obj.Gross_Rated_Heating_Capacity
        heating_COP = AC_VariableRefFlow_obj.Gross_Rated_Heating_COP
        if cooling_Cap == "Autosize" or heating_Cap == "Autosize" or cooling_Cap == "autosize" or heating_Cap == "autosize":
            autosize_arr.append(AC_VariableRefFlow_obj.Heat_Pump_Name)
        else:
            cooling_Cap_KW = cooling_Cap / 1000
            heating_Cap_KW = heating_Cap / 1000
            ECBC_Cooling_COP = get_ECBC_COP('VRF_A', cooling_Cap_KW, "EER", compliance) # 'ECBC'
            ECBC_Heating_COP = get_ECBC_COP('VRF_A', heating_Cap_KW, "EER", compliance)
            if cooling_COP >= ECBC_Cooling_COP and heating_COP >= ECBC_Heating_COP:
                print AC_VariableRefFlow_obj.Heat_Pump_Name + " Meet"
            else:
                Equiment_name_arr.append(AC_VariableRefFlow_obj.Heat_Pump_Name)

            # Evaluate System C Efficiency  - Chiller:Electric:EIR
    Chiller_Electric_EIR_objs = Proposed_Idf.idfobjects["Chiller:Electric:EIR".upper()]
    for Chiller_Electric_EIR_obj in Chiller_Electric_EIR_objs:
        reference_Cap = Chiller_Electric_EIR_obj.Reference_Capacity
        reference_COP = Chiller_Electric_EIR_obj.Reference_COP
        condenser_Type = Chiller_Electric_EIR_obj.Condenser_Type
        if reference_Cap == "Autosize" or reference_Cap == "autosize":
            autosize_arr.append(Chiller_Electric_EIR_obj.Name)
        else:
            reference_Cap_KW = reference_Cap / 1000
            if condenser_Type == "WaterCooled":
                ECBC_COP = get_ECBC_COP('WCC', reference_Cap_KW, "COP", compliance)
            if condenser_Type == "AirCooled":
                ECBC_COP = get_ECBC_COP('ACC', reference_Cap_KW, "COP", compliance)

            if reference_COP >= ECBC_COP:
                print Chiller_Electric_EIR_obj.Name + " Meet"
            else:
                Equiment_name_arr.append([Chiller_Electric_EIR_obj.Name,reference_COP, ECBC_COP] )

            # Evaluate System C Efficiency  - Chiller:Electric:ReformulatedEIR
    Chiller_Electric_REIR_objs = Proposed_Idf.idfobjects["Chiller:Electric:ReformulatedEIR".upper()]
    for Chiller_Electric_REIR_obj in Chiller_Electric_REIR_objs:
        reference_Cap = Chiller_Electric_REIR_obj.Reference_Capacity
        reference_COP = Chiller_Electric_REIR_obj.Reference_COP
        if reference_Cap == "Autosize" or reference_Cap == "autosize":
            autosize_arr.append(Chiller_Electric_REIR_obj.Name)
        else:
            reference_Cap_KW = reference_Cap / 1000
            ECBC_COP = get_ECBC_COP('WCC', reference_Cap_KW, "COP", compliance)
            if reference_COP >= ECBC_COP:
                print Chiller_Electric_REIR_obj.Name + " Meet"
            else:
                Equiment_name_arr.append([Chiller_Electric_REIR_obj.Name, reference_COP, ECBC_COP])

            # Evaluate System C Efficiency  - Chiller:Electric
    Chiller_Electric_objs = Proposed_Idf.idfobjects["Chiller:Electric".upper()]
    for Chiller_Electric_obj in Chiller_Electric_objs:
        nominal_Cap = Chiller_Electric_obj.Nominal_Capacity
        nominal_COP = Chiller_Electric_obj.Nominal_COP
        condenser_Type = Chiller_Electric_obj.Condenser_Type
        if nominal_Cap == "Autosize" or nominal_Cap == "autosize":
            autosize_arr.append(Chiller_Electric_obj.Name)
        else:
            nominal_Cap_KW = nominal_Cap / 1000
            if condenser_Type == "WaterCooled":
                ECBC_COP = get_ECBC_COP('WCC', nominal_Cap_KW, "COP", compliance)
            if condenser_Type == "AirCooled":
                ECBC_COP = get_ECBC_COP('ACC', nominal_Cap_KW, "COP", compliance)

            if nominal_COP >= ECBC_COP:
                print Chiller_Electric_obj.Name + " Meet"
            else:
                Equiment_name_arr.append([Chiller_Electric_obj.Name, nominal_COP, ECBC_COP])

    # Evaluate Efficiency  - Boiler:HotWater
    Boiler_HW_objs = Proposed_Idf.idfobjects["Boiler:HotWater".upper()]
    for Boiler_HW_obj in Boiler_HW_objs:
        nominal_Cap = Boiler_HW_obj.Nominal_Capacity
        nominal_COP = Boiler_HW_obj.Nominal_Thermal_Efficiency
        fuel_Type = Boiler_HW_obj.Fuel_Type
        if nominal_Cap == "Autosize" or nominal_Cap == "autosize":
            autosize_arr.append(Boiler_HW_obj.Name)
        else:
            if fuel_Type != "Electricity":
                if compliance == 'ECBC':
                    ECBC_COP = 0.8
                else:
                    ECBC_COP = 0.85

            if nominal_COP >= ECBC_COP:
                print Boiler_HW_obj.Name + " Meet"
            else:
                Equiment_name_arr.append([Boiler_HW_obj.Name, nominal_COP, ECBC_COP])


    # Evaluate Efficiency  - Boiler:Steam
    Boiler_S_objs = Proposed_Idf.idfobjects["Boiler:Steam".upper()]
    for Boiler_S_obj in Boiler_S_objs:
        nominal_Cap = Boiler_S_obj.Nominal_Capacity
        nominal_COP = Boiler_S_obj.Theoretical_Efficiency
        fuel_Type = Boiler_S_obj.Fuel_Type
        if nominal_Cap == "Autosize" or nominal_Cap == "autosize":
            autosize_arr.append(Boiler_S_obj.Name)
        else:
            if fuel_Type != "Electricity":
                if compliance == 'ECBC':
                    ECBC_COP = 0.8
                else:
                    ECBC_COP = 0.85

            if nominal_COP >= ECBC_COP:
                print Boiler_S_obj.Name + " Meet"
            else:
                Equiment_name_arr.append([Boiler_S_obj.Name, nominal_COP, ECBC_COP])
    status_msg = ""
    if len(autosize_arr) > 0:
        status_msg = "Error 1: below mention object's capacity are set to autosize. Kindly change this to capacity value\n"
        for autosize_name in autosize_arr:
            status_msg +=  autosize_name +", "
    if len(Equiment_name_arr)> 0 :
        status_msg += "\nError 2: below mention objects not meet " + compliance + " COP\n"
        for equipment_name in Equiment_name_arr:
            status_msg += "\t" + equipment_name[0] + " having COP " + str(equipment_name[1]) + " and expected COP should be above " + str(equipment_name[2]) + "\n"
    return status_msg

def COP_AfterincludingFanPower(ModeledCap_KWr, ModeledCOP):
    ModeledCapTR = ModeledCap_KWr * 3413 / 12000
    AirFlowKW = (ModeledCapTR * 400 * 0.365) / 1000
    ModeledCapkWe = ModeledCap_KWr / ModeledCOP
    EqpElecCap = ModeledCapkWe + AirFlowKW
    Proposed_COP = ModeledCap_KWr / EqpElecCap
    return Proposed_COP


def get_ECBC_COP(type_code, capacity, column_nm, compliance):
    tbl_rows = get_constants(table='Tbl_5_1_Eff', Type=type_code, Compliance=compliance)
    # print tbl_rows
    CoolingCOP = 0
    for y in range(0, len(tbl_rows)):
        # print tbl_rows[y]["Max_kWr_"]
        if tbl_rows[y]["Max_kWr_"] != '':
            if float(tbl_rows[y]["Min_kWr_"]) < float(capacity) <= float(tbl_rows[y]["Max_kWr_"]):
                CoolingCOP = float(tbl_rows[y][column_nm])
                break
        else:
            if capacity > float(tbl_rows[y]["Min_kWr_"]):
                CoolingCOP = float(tbl_rows[y][column_nm])
                break
    #print str(capacity) + ' ' + column_nm + ' ' + str(CoolingCOP)
    return CoolingCOP


def validate_proposed(Proposed_Idf, BUA, Compliance):
    status_msg = CoolingTowerFan_Validation(Proposed_Idf, BUA)
    status_msg += Eval_Proposed_System_Efficiency(Proposed_Idf, Compliance)
    return status_msg

# # idf = "F:/TestBEP/180218_C/High-rise_revise_v8.idf"
# idf = "F:/TestBEP/180222_2/Office(10-30).idf"
# iddfile = "C:/EnergyPlusV8-6-0/Energy+.idd"
# IDF.setiddname(iddfile)
# Proposed_Idf = IDF(idf)
# CoolingTowerFan_Validation(Proposed_Idf, 25000)
# status_msg = Eval_Proposed_System_Efficiency(Proposed_Idf, 'ECBC')
# print status_msg

# Set_VAV_Fan_Efficiency(Html_path, ECBC_standard_Idf)