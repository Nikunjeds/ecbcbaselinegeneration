# -*- coding: utf-8 -*-
"""
Edits file for U-factor Exception for a) Opaque assembly as/4.3.2 and b) Vertical fenestration assembly as/4.3.3.1 (Table 4-14) for Vertical Fenestrations
The script accepts Building Classification (BuildingClass), Climate Zone (CZ) and Latitude (lat) as arguments
"""
from eppy import modeleditor
from eppy.modeleditor import IDF


# iddfile = "C:/EnergyPlusV8-6-0/Energy+.idd"
# idffile = "F:/1753 BEP-EMIS Tool/03 Design/04 EnergyModel MockUps/171217/Proposed/Proposed.idf"
# IDF.setiddname(iddfile)
# Proposed_IDF = IDF(idffile)
# BuildingClass = "Ed-Insti"
#
# # print (Typ)
# CZ = "WH"
# lat = 20  # pass as a parameter to the code.

def checkException(ECBC_standard_Idf, BuildingClass, CZ, lat, north_axis, zone_wise_buildingstory_details):
    Typ = BuildingClass[0:BuildingClass.find("-")]
    if CZ != "Cold":
        # Reading Zones
        Zones = ECBC_standard_Idf.idfobjects['ZONE']
        SizingZones = [row[1] for row in zone_wise_buildingstory_details if row[3] == "conditioned"]
            #ECBC_standard_Idf.idfobjects['SIZING:ZONE']
        AllZones = set([])
        CondZones = set([])

        # Making sets for Conditioned, Unconditioned and External Zones
        for zone in Zones:
            AllZones.add(zone.Name)
        for zone in SizingZones:
            CondZones.add(
                zone) # Zone_or_ZoneList_Name)  # Will all SizingZones by individual zones, OR can they be ZoneLists as well?
        UnCondZones = AllZones - CondZones
        UnCondExtZones = set([])
        BuildingSurface = ECBC_standard_Idf.idfobjects['BUILDINGSURFACE:DETAILED']
        ExtZones = set([])
        for surface in BuildingSurface:
            if surface.Outside_Boundary_Condition == 'Outdoors':
                ExtZones.add(surface.Zone_Name)

        # Finding intersection of Exterior and Unconditioned Zones. Conditional for removing from larger subset.
        if len(ExtZones) > len(UnCondZones):
            TempZones = ExtZones - UnCondZones
            UnCondExtZones = ExtZones - TempZones
        else:
            TempZones = UnCondZones - ExtZones
            UnCondExtZones = UnCondZones - TempZones
        # Checking if glazed surfaces exist in Unconditioned Zones
        GlazSurface = set([])
        FenestrationSurface = ECBC_standard_Idf.idfobjects['FENESTRATIONSURFACE:DETAILED']
        for surface in FenestrationSurface:
            GlazSurface.add(surface.Name)
        for zone in UnCondExtZones:
            for surface in BuildingSurface:
                if surface.Zone_Name == zone and surface.Surface_Type == 'Wall' and surface.Outside_Boundary_Condition == 'Outdoors':
                    # Checking for Opaque assembly exception
                    if Typ != "HC" and BuildingClass != "Ed-Sch" and BuildingClass != "Ho-Ho":
                        surface.Construction_Name = "ECBC_wall_0.8"
                        print(surface.Name, " : ", "Opaque Assembly U factor Exception Applied")
                    # Checking for Vertical Fenestration assembly exception
                    for glazsurface in FenestrationSurface:
                        if surface.Name == glazsurface.Building_Surface_Name:
                            azimuth = glazsurface.azimuth + north_axis
                            if azimuth > 360:
                                azimuth = azimuth - 360
                            # Checking Glazing Orientation
                            if azimuth <= 45 and azimuth >= 315:  # Glz North
                                if lat > 15:
                                    glazsurface.Construction_Name = 'ECBC_glazingUnconditioned'  # U: 5, SHGC: 0.27, VLT: 0.27 Note: Refers Max. EFFECTIVE SHGC
                                else:
                                    glazsurface.Construction_Name = 'ECBC_glazingUnconditioned'  # U: 5, SHGC: 0.27, VLT: 0.27 Note: Refers Max. EFFECTIVE SHGC
                            else:  # Glz Non North
                                # skipping PF >= 0.4 Check
                                glazsurface.Construction_Name = 'ECBC_glazingUnconditioned'  # U: 5, SHGC: 0.27, VLT: 0.27 Note: Refers Max. EFFECTIVE SHGC
                            print(glazsurface.Name, " : ", glazsurface.Construction_Name, " : ",
                                  "Vertical Fenestration Assembly U factor Exception Applied ")
    else:
        pass
