from Create_HVAC_Zone_Array import remove_Object_From_CSV
from UpdateBuildingEnvelope import copy_objects
"""helper function to create HVAC System type A"""

#function to craete a HVAC sizing zone list with coresponding values (Design_Specification_Outdoor_Air_Object_Name, Design_Specification_Zone_Air_Distribution_Object_Name, fan schedule, heating setpoint and cooling setpoint) 
def get_HVAC_Zone_List(ECBC_standard_Idf, System_Zonelist, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details):
    #array to store zone wise information and to store unique schedule name
    HVAC_Zone_List = []
    HVAC_unique_schedule = []
    
    for Zonelist in  System_Zonelist:
        #print Zonelist
        
        #find zonelist name from zone_wise_buildingstory_details array 
     
        zone_names = [row[1] for row in zone_wise_buildingstory_details if row[0] == Zonelist and row[3] == "conditioned"]   
        #print zone_names
        # find row containg zone detail from ECBC_Zonelist_Wise_Schedule array
        row = [schedule_row for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == Zonelist]
        #read all required data
        ECBC_fanSchedule =row[0][7]
        ECBC_heatingSetpoint =row[0][8]
        ECBC_coolingSetpoint =row[0][9]
        for zones in zone_names:
              
#       #store value in temp array and append this arry to original array    
            HVAC_Zone_Details = [zones, ECBC_fanSchedule, ECBC_heatingSetpoint, ECBC_coolingSetpoint]
            HVAC_Zone_List.append(HVAC_Zone_Details)
###        print sizing_zone.Zone_or_ZoneList_Name
###        print sizing_zone.Design_Specification_Outdoor_Air_Object_Name
###        print sizing_zone.Design_Specification_Zone_Air_Distribution_Object_Name
##        
#        
    #print HVAC_Zone_List
    return HVAC_Zone_List 


#function to add sizing parameter object
def add_Sizing_Parameter(ECBC_standard_Idf, ECBC_PTHP_Library, heating_Sizing_Factor, cooling_Sizing_Factor):
    print "Creating Sizing:Parameter object...."
    #copy sizing parameter object from standard idf to ECBC_standard_Idf and set its attribute value and save
    sizing_parameter_objs = ECBC_PTHP_Library.idfobjects["Sizing:Parameters".upper()][0]
    sizing_parameter_objs.Heating_Sizing_Factor=heating_Sizing_Factor
    sizing_parameter_objs.Cooling_Sizing_Factor=cooling_Sizing_Factor
    sizing_parameter_objs.Timesteps_in_Averaging_Window=""
    ECBC_standard_Idf.copyidfobject(sizing_parameter_objs)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf


#function to add one HVAC Thermostat object 
def add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_PTHP_Library, zone_name, ECBC_heatingSetpoint, ECBC_coolingSetpoint):
    print "Creating HVACTemplate:Thermostat object...."
    #read HVAC thermostat object from standard idf
    new_thermostat_obj = ECBC_PTHP_Library.idfobjects["HVACTemplate:Thermostat".upper()][0]
   # ECBC_standard_Idf = copy_objects(ECBC_PTHP_Library, ECBC_standard_Idf, "HVACTemplate:Thermostat".upper())
   # hvac_thermostat_objs = ECBC_standard_Idf.idfobjects["HVACTemplate:Thermostat".upper()][0]
    #set schedule from csv
    #set attribute value of objects
    Thermostat_Name = zone_name + "_Thermostat"
    new_thermostat_obj.Name=Thermostat_Name
    new_thermostat_obj.Heating_Setpoint_Schedule_Name = ECBC_heatingSetpoint
    new_thermostat_obj.Cooling_Setpoint_Schedule_Name = ECBC_coolingSetpoint
    new_thermostat_obj.Constant_Heating_Setpoint = ""
    new_thermostat_obj.Constant_Cooling_Setpoint = ""
    #copy that object to ECBC_standard_Idf
    ECBC_standard_Idf.copyidfobject(new_thermostat_obj)
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf

#function to add one HVAC zone PTHP object 
def add_HVAC_Zone_PTHP(ECBC_standard_Idf, ECBC_PTHP_Library, zone_name, ECBC_fanSchedule, cooling_SetPoint):
    
    print "Creating " + zone_name  + " HVACTemplate:Zone:PTHP..."
    Thermostat_Name = zone_name + "_Thermostat"
    #read HVAC zone PTHP object from standard idf
    new_PTHP_obj = ECBC_PTHP_Library.idfobjects["HVACTemplate:Zone:PTHP".upper()][0]
    new_PTHP_obj.Zone_Name = zone_name
    new_PTHP_obj.Template_Thermostat_Name = Thermostat_Name
    new_PTHP_obj.System_Availability_Schedule_Name = ECBC_fanSchedule
    new_PTHP_obj.Zone_Cooling_Design_Supply_Air_Temperature = cooling_SetPoint - 11
    
    ECBC_standard_Idf.copyidfobject(new_PTHP_obj)

    ECBC_standard_Idf.save()
    # replace fan / Hvac schedule  
    return ECBC_standard_Idf

def creating_HVAC_SystemType_A(ECBC_standard_Idf, System_Zonelist, ECBC_PTHP_Library, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details, cooling_SetPoint):
    print "get HVAC Zone List"
    HVAC_Zone_List = get_HVAC_Zone_List(ECBC_standard_Idf, System_Zonelist, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    #ECBC_standard_Idf = remove_Object_From_CSV(ECBC_standard_Idf)
    #ECBC_standard_Idf = add_Sizing_Parameter(ECBC_standard_Idf, ECBC_PTHP_Library, heating_Sizing_Factor, cooling_Sizing_Factor)
    
   
    for zone_details in HVAC_Zone_List:
        ECBC_standard_Idf = add_HVAC_Thermostat(ECBC_standard_Idf, ECBC_PTHP_Library, zone_details[0],zone_details[2],zone_details[3])
        ECBC_standard_Idf = add_HVAC_Zone_PTHP(ECBC_standard_Idf, ECBC_PTHP_Library, zone_details[0], zone_details[1], cooling_SetPoint)
    return ECBC_standard_Idf
    print "HVAC System A setup"
   