!-Generator IDFEditor 1.48
!-Option OriginalOrderTop UseSpecialFormat

!-NOTE: All comments with '!-' are ignored by the IDFEditor and are generated automatically.
!-      Use '!' comments if they need to be retained when using the IDFEditor.

Sizing:Parameters,
    1.25,                    !- Heating Sizing Factor
    1.15;                    !- Cooling Sizing Factor

HVACTemplate:Thermostat,
    All Zones,               !- Name
    Htg-SetP-Sch,            !- Heating Setpoint Schedule Name
    ,                        !- Constant Heating Setpoint {C}
    Clg-SetP-Sch,            !- Cooling Setpoint Schedule Name
    ;                        !- Constant Cooling Setpoint {C}

HVACTemplate:Zone:VAV,
    SPACE1-1,                !- Zone Name
    VAV Sys 1,               !- Template VAV System Name
    All Zones,               !- Template Thermostat Name
    autosize,                !- Supply Air Maximum Flow Rate {m3/s}
    ,                        !- Zone Heating Sizing Factor
    ,                        !- Zone Cooling Sizing Factor
    Constant,                !- Zone Minimum Air Flow Input Method
    0.3,                     !- Constant Minimum Air Flow Fraction
    ,                        !- Fixed Minimum Air Flow Rate {m3/s}
    ,                        !- Minimum Air Flow Fraction Schedule Name
    DetailedSpecification,   !- Outdoor Air Method
    ,                        !- Outdoor Air Flow Rate per Person {m3/s}
    ,                        !- Outdoor Air Flow Rate per Zone Floor Area {m3/s-m2}
    ,                        !- Outdoor Air Flow Rate per Zone {m3/s}
    None,                    !- Reheat Coil Type
    ,                        !- Reheat Coil Availability Schedule Name
    Reverse,                 !- Damper Heating Action
    ,                        !- Maximum Flow per Zone Floor Area During Reheat {m3/s-m2}
    ,                        !- Maximum Flow Fraction During Reheat
    ,                        !- Maximum Reheat Air Temperature {C}
    ,                        !- Design Specification Outdoor Air Object Name for Control
    ,                        !- Supply Plenum Name
    ,                        !- Return Plenum Name
    None,                    !- Baseboard Heating Type
    ,                        !- Baseboard Heating Availability Schedule Name
    autosize,                !- Baseboard Heating Capacity {W}
    SystemSupplyAirTemperature,  !- Zone Cooling Design Supply Air Temperature Input Method
    ,                        !- Zone Cooling Design Supply Air Temperature {C}
    ,                        !- Zone Cooling Design Supply Air Temperature Difference {deltaC}
    SupplyAirTemperature,    !- Zone Heating Design Supply Air Temperature Input Method
    50.0,                    !- Zone Heating Design Supply Air Temperature {C}
    ;                        !- Zone Heating Design Supply Air Temperature Difference {deltaC}

HVACTemplate:System:VAV,
    VAV Sys 1,               !- Name
    FanAvailSched,           !- System Availability Schedule Name
    autosize,                !- Supply Fan Maximum Flow Rate {m3/s}
    autosize,                !- Supply Fan Minimum Flow Rate {m3/s}
    0.54,                    !- Supply Fan Total Efficiency
    622,                     !- Supply Fan Delta Pressure {Pa}
    0.9,                     !- Supply Fan Motor Efficiency
    1,                       !- Supply Fan Motor in Air Stream Fraction
    ChilledWater,            !- Cooling Coil Type
    ,                        !- Cooling Coil Availability Schedule Name
    ,                        !- Cooling Coil Setpoint Schedule Name
    12.8,                    !- Cooling Coil Design Setpoint {C}
    Electric,                !- Heating Coil Type
    ,                        !- Heating Coil Availability Schedule Name
    ,                        !- Heating Coil Setpoint Schedule Name
    10.0,                    !- Heating Coil Design Setpoint {C}
    0.8,                     !- Gas Heating Coil Efficiency
    0.0,                     !- Gas Heating Coil Parasitic Electric Load {W}
    None,                    !- Preheat Coil Type
    ,                        !- Preheat Coil Availability Schedule Name
    ,                        !- Preheat Coil Setpoint Schedule Name
    ,                        !- Preheat Coil Design Setpoint {C}
    0.8,                     !- Gas Preheat Coil Efficiency
    0.0,                     !- Gas Preheat Coil Parasitic Electric Load {W}
    autosize,                !- Maximum Outdoor Air Flow Rate {m3/s}
    autosize,                !- Minimum Outdoor Air Flow Rate {m3/s}
    FixedMinimum,            !- Minimum Outdoor Air Control Type
    ,                        !- Minimum Outdoor Air Schedule Name
    FixedDryBulb,            !- Economizer Type
    NoLockout,               !- Economizer Lockout
    24,                      !- Economizer Upper Temperature Limit {C}
    4,                       !- Economizer Lower Temperature Limit {C}
    ,                        !- Economizer Upper Enthalpy Limit {J/kg}
    ,                        !- Economizer Maximum Limit Dewpoint Temperature {C}
    ,                        !- Supply Plenum Name
    ,                        !- Return Plenum Name
    DrawThrough,             !- Supply Fan Placement
    ASHRAE90.1-2004AppendixG,!- Supply Fan Part-Load Power Coefficients
    CycleOnAny,              !- Night Cycle Control
    ,                        !- Night Cycle Control Zone Name
    None,                    !- Heat Recovery Type
    0.70,                    !- Sensible Heat Recovery Effectiveness
    0.65,                    !- Latent Heat Recovery Effectiveness
    None,                    !- Cooling Coil Setpoint Reset Type
    None,                    !- Heating Coil Setpoint Reset Type
    None,                    !- Dehumidification Control Type
    ,                        !- Dehumidification Control Zone Name
    60.0,                    !- Dehumidification Setpoint {percent}
    None,                    !- Humidifier Type
    ,                        !- Humidifier Availability Schedule Name
    0.000001,                !- Humidifier Rated Capacity {m3/s}
    2690.0,                  !- Humidifier Rated Electric Power {W}
    ,                        !- Humidifier Control Zone Name
    30.0,                    !- Humidifier Setpoint {percent}
    NonCoincident,           !- Sizing Option
    ,                        !- Return Fan
    ,                        !- Return Fan Total Efficiency
    ,                        !- Return Fan Delta Pressure {Pa}
    ,                        !- Return Fan Motor Efficiency
    ,                        !- Return Fan Motor in Air Stream Fraction
    ;                        !- Return Fan Part-Load Power Coefficients

HVACTemplate:Plant:ChilledWaterLoop,
    Chilled Water Loop,      !- Name
    ,                        !- Pump Schedule Name
    INTERMITTENT,            !- Pump Control Type
    Default,                 !- Chiller Plant Operation Scheme Type
    ,                        !- Chiller Plant Equipment Operation Schemes Name
    ,                        !- Chilled Water Setpoint Schedule Name
    7.22,                    !- Chilled Water Design Setpoint {C}
    ConstantPrimaryVariableSecondary,  !- Chilled Water Pump Configuration
    179352,                  !- Primary Chilled Water Pump Rated Head {Pa}
    179352,                  !- Secondary Chilled Water Pump Rated Head {Pa}
    Default,                 !- Condenser Plant Operation Scheme Type
    ,                        !- Condenser Equipment Operation Schemes Name
    SpecifiedSetpoint,       !- Condenser Water Temperature Control Type
    ,                        !- Condenser Water Setpoint Schedule Name
    29.4,                    !- Condenser Water Design Setpoint {C}
    179352,                  !- Condenser Water Pump Rated Head {Pa}
    None,                    !- Chilled Water Setpoint Reset Type
    12.2,                    !- Chilled Water Setpoint at Outdoor Dry-Bulb Low {C}
    15.6,                    !- Chilled Water Reset Outdoor Dry-Bulb Low {C}
    6.7,                     !- Chilled Water Setpoint at Outdoor Dry-Bulb High {C}
    26.7,                    !- Chilled Water Reset Outdoor Dry-Bulb High {C}
    PumpPerChiller,          !- Chilled Water Primary Pump Type
    ,                        !- Chilled Water Secondary Pump Type
    PumpPerTower,            !- Condenser Water Pump Type
    ,                        !- Chilled Water Supply Side Bypass Pipe
    ,                        !- Chilled Water Demand Side Bypass Pipe
    ,                        !- Condenser Water Supply Side Bypass Pipe
    ,                        !- Condenser Water Demand Side Bypass Pipe
    Water,                   !- Fluid Type
    ,                        !- Loop Design Delta Temperature {deltaC}
    7.22,                    !- Minimum Outdoor Dry Bulb Temperature {C}
    UniformLoad,             !- Chilled Water Load Distribution Scheme
    UniformLoad;             !- Condenser Water Load Distribution Scheme

HVACTemplate:Plant:Chiller,
    Main Chiller,            !- Name
    ElectricCentrifugalChiller,  !- Chiller Type
    autosize,                !- Capacity {W}
    6.1,                     !- Nominal COP {W/W}
    WaterCooled,             !- Condenser Type
    1,                       !- Priority
    ;                        !- Sizing Factor

HVACTemplate:Plant:Tower,
    Main Tower,              !- Name
    TwoSpeed,                !- Tower Type
    autosize,                !- High Speed Nominal Capacity {W}
    autosize,                !- High Speed Fan Power {W}
    autosize,                !- Low Speed Nominal Capacity {W}
    autosize,                !- Low Speed Fan Power {W}
    autosize,                !- Free Convection Capacity {W}
    1,                       !- Priority
    ;                        !- Sizing Factor

