#pre run

from eppy import modeleditor
from eppy.modeleditor import IDF

from UpdateSimulationControl import removeObjects

def get_Zone_wise_Light_Details(ECBC_standard_Idf ):
    zone_wise_lights_arr = []
    Lights_objs = ECBC_standard_Idf.idfobjects["lights".upper()]
    for lights_obj in Lights_objs:
        zonelist_name  = lights_obj.Zone_or_ZoneList_Name
        schedule_name = lights_obj.Schedule_Name
        wats_per_zone_floor_area = lights_obj.Watts_per_Zone_Floor_Area
        ZoneList_obj = ECBC_standard_Idf.getobject('Zonelist'.upper(), zonelist_name)
        for fieldname in ZoneList_obj.fieldnames:
            if fieldname.startswith("Zone") and ZoneList_obj[fieldname] != "":
                zone_name = ZoneList_obj[fieldname]
                zone_area = modeleditor.zonearea(ECBC_standard_Idf, zone_name)
                zone_lights_details = [zonelist_name, zone_name, schedule_name, wats_per_zone_floor_area, zone_area]
                zone_wise_lights_arr.append(zone_lights_details)

    return  zone_wise_lights_arr       

def add_lights_object_Zone_Wise (ECBC_standard_Idf, zone_wise_lights_arr):
    for lights_details in zone_wise_lights_arr:
        print "Adding " + lights_details[1] + "  Lights Object...."
        new_lights_obj = ECBC_standard_Idf.newidfobject("Lights".upper())
        new_lights_obj.Name = lights_details[1]+ "_Lights"
        new_lights_obj.Zone_or_ZoneList_Name = lights_details[1]
        new_lights_obj.Schedule_Name = lights_details[2]
        new_lights_obj.Design_Level_Calculation_Method = "Watts/Area"
        new_lights_obj.Watts_per_Zone_Floor_Area = lights_details[3]
        ECBC_standard_Idf.save()
    print "All Lights object Added."
    return ECBC_standard_Idf

def update_Zone_Floor_Area_value(ECBC_standard_Idf, Lights_obj_name):
    Lights_obj = ECBC_standard_Idf.getobject('Lights'.upper(),Lights_obj_name )
    Lights_obj.Watts_per_Zone_Floor_Area = float(Lights_obj.Watts_per_Zone_Floor_Area) * 0.9
    ECBC_standard_Idf.save()
    return ECBC_standard_Idf
    
def update_Zone_Floor_Area(ECBC_standard_Idf, BuiltUpArea, ECBC_Zonelist_Wise_Schedule, zone_wise_lights_arr, typology):
    if BuiltUpArea <= 20000:
        zonelist_conference = [row for row in zone_wise_lights_arr if row[0].find("Conference") != -1]
        if (len(zonelist_conference) > 0):
            for zonelist_conference_details in zonelist_conference:
                Lights_obj_name = zonelist_conference_details[1]+"_Lights"
                print Lights_obj_name + " updating"
                update_Zone_Floor_Area_value(ECBC_standard_Idf, Lights_obj_name)
            print "All Zones of Conference updated"
        else:
            print "No Conference Zonelist in IDF"
    else:
        #incomplete logic
        Zonelist_objs = ECBC_standard_Idf.idfobjects["zonelist".upper()]
        for zonelist_obj in Zonelist_objs:
            #habitable zone
            habitable_status = [schedule_row[13] for schedule_row in ECBC_Zonelist_Wise_Schedule if schedule_row[0] == zonelist_obj.Name]
            if habitable_status[0] == 'Y':
                zonelist_habitable = [row for row in zone_wise_lights_arr if row[0]== zonelist_obj.Name]
                if (len(zonelist_habitable) > 0):
                    for zonelist_habitable_details in zonelist_habitable:
                        Lights_obj_name = zonelist_habitable_details[1]+"_Lights"
                        zone_area = zonelist_habitable_details[4]
                        if zone_area<30:
                            update_Zone_Floor_Area_value(ECBC_standard_Idf, Lights_obj_name)
                    print "All Habitable Zones updated"
                else:
                     print "No Habitable Zonelist in IDF"
            
            #Storage zonelist
            zonelist_storage = [row for row in zone_wise_lights_arr if row[0].find("Storage") != -1]
            if (len(zonelist_storage) > 0):
                for zonelist_storage_details in zonelist_storage:
                    Lights_obj_name = zonelist_storage_details[1]+"_Lights"
                    zone_area = zonelist_storage_details[4]
                    if zone_area>25:
                        update_Zone_Floor_Area_value(ECBC_standard_Idf, Lights_obj_name)
                print "All Storage Zones updated"
            else:
                print "No Storage Zonelist in IDF"
            
            #restroom zonelist 
            zonelist_restroom = [row for row in zone_wise_lights_arr if row[0].find("Restroom") != -1]
            if (len(zonelist_restroom) > 0):
                for zonelist_restroom_details in zonelist_restroom:
                    Lights_obj_name = zonelist_restroom_details[1]+"_Lights"
                    zone_area = zonelist_restroom_details[4]
                    if zone_area>15:
                        update_Zone_Floor_Area_value(ECBC_standard_Idf, Lights_obj_name)
                print "All Restroom Zones updated"
            else:
                print "No Restroom Zonelist in IDF"
            # conference zonelist
            zonelist_conference = [row for row in zone_wise_lights_arr if row[0].find("Conference") != -1]
            if (len(zonelist_conference) > 0):
                for zonelist_conference_details in zonelist_conference:
                    Lights_obj_name = zonelist_conference_details[1]+"_Lights"
                    zone_area = zonelist_conference_details[4]
                    if zone_area>=30:
                        update_Zone_Floor_Area_value(ECBC_standard_Idf, Lights_obj_name)
                print "All Conference Zones updated"
            else:
                print "No Conference Zonelist in IDF"
            
            #Hospitality typology check
            if typology == "Hospitality":
                zonelist_corridor = [row for row in zone_wise_lights_arr if row[0].find("Corridor") != -1]
                if (len(zonelist_corridor) > 0):
                    for zonelist_corridor_details in zonelist_corridor:
                        Lights_obj_name = zonelist_corridor_details[1]+"_Lights"
                        update_Zone_Floor_Area_value(ECBC_standard_Idf, Lights_obj_name)
                    print "All Corridor Zones updated"
                else:
                    print "No Corridor Zonelist in IDF"
    return ECBC_standard_Idf            
    #else:    
    

def set_LightingControls(ECBC_standard_Idf,BuiltUpArea,ECBC_Zonelist_Wise_Schedule,typology ):
    
    zone_wise_lights_arr = get_Zone_wise_Light_Details(ECBC_standard_Idf)

    ECBC_standard_Idf = removeObjects(ECBC_standard_Idf,"Lights")
    print "setting Lights zone wise"
    ECBC_standard_Idf  = add_lights_object_Zone_Wise (ECBC_standard_Idf, zone_wise_lights_arr)
    ECBC_standard_Idf = update_Zone_Floor_Area(ECBC_standard_Idf, BuiltUpArea, ECBC_Zonelist_Wise_Schedule, zone_wise_lights_arr, typology)
    return ECBC_standard_Idf



