"""main file to call all function
    1.	Copy  baselineIDF from Proposed
    2.	Read Info
        a.	Read_ECBC_List.py
        b.	Building_Story.py
    3.	Set_Validation_Check
        a.	CheckZoneList.py
        b.	Validity_check of e+ ( view-> validity check [ctrl+R])
    4.	Set_simulationControl
        a.	update_SimulationControl
        b.	update_SizingPeriodDesignDay_TimeStep
        c.	set_SizingPeriod
        d.	update_RunPeriod
        e.	removeObjects 
    5.	Envelope_Parameters
        a.	removeShading
        b.	copy all Materials, construction from Construction_Library to baseline_IDF
        c.	Replace ECBC_Construction to Wall, Window, Roof, Skylight in baseline_IDF
        d.	Check and Update if WWR > 40 and SRR > 5 
    6.	Lighting & Schedule
        a.	Update Lighting Power Density in baseline_IDF 
        b.	Copy Schedule_Library to baseline_IDF
        c.	Update schedule for People, Lights, Equipment from Schedule_Library
    7.	Map_System
        a.	System mapping according to [zonelist, conditioned area, building type] 
    8.	Apply System
        a.	HVAC SystemType A
        b.	HVAC SystemType B
        c.	HVAC SystemType C
        d.	HVAC SystemType D
    9.	Sizing Run & Update HVAC Parameter
        a.	Running simulation and based on output update below list
        b.	Fan Efficiency
        c.	Pump Efficiency
        d.	Efficiency of System
    10.	Update Proposed IDF
        a.	Update schedule [people, lights, equipment, thermostat set point, HVAC ON/OFF, fan on/off]
"""

import os
import time
import shutil
from eppy import modeleditor
from eppy.modeleditor import IDF
from CheckProposed import validate_proposed
from Generate_List_From_CSV import get_ECBC_Zonelist
from Building_Story import generate_Building_Story
from CheckZoneList import zonelistCheck
from CheckLighting import update_lighting
from CheckWWR import manage_WWR_SRR
from UpdateBuildingEnvelope import update_Envelope_Parameter
from UpdateSchedule import update_schedule
from UpdateSchedule import remove_HVACTemplate_Schedule
from UpdateSimulationControl import fn_simulationControl
from UpdateOutputControlReportingTolerances import update_outputControl_ReportingTolerance
from SetLightingControl import set_LightingControls
from HVAC_SystemType_A import creating_HVAC_SystemType_A
from HVAC_SystemType_B import creating_HVAC_SystemType_B
from HVAC_SystemType_C import creating_HVAC_SystemType_C
from HVAC_SystemType_D import creating_HVAC_SystemType_D
from RunSimulation.run_Eplus import runEplus
from PostRunProcessing import post_Run_Processing_System_A
from PostRunProcessing import post_Run_Processing_System_B
from PostRunProcessing_SystemC import post_Run_Processing_System_C
from SystemAssignment import System_type_assignment
from ModifyProposed import modify_Proposed
from CheckEconEnergyApplicability import Check_Econ_EnergyRecov_Applicabilty
from SetVAVFanEfficiency import Set_VAV_Fan_Efficiency


# function to create Standard_ECBC_IDF from Proposed_IDF
def Create_Standard_IDF_From_Proposed(proposed_Idf):
    # Generate path of Standard_ECBC_IDF and copy proposed to Standard_ECBC_IDF
    folder = os.path.dirname(proposed_Idf)
    standard_ECBC_Idf = os.path.join(folder, "baseline.idf")
    # Create copy of Proposed idf to Standard_ECBC_IDF
    shutil.copyfile(proposed_Idf, standard_ECBC_Idf)
    return standard_ECBC_Idf


# function to manage all function call
def initialize_Baseline_Generation(IDF_P, wall_U, roof_U, window_North_SHGC, window_NonNorth_SHGC, cooling_SetPoint,
                                   heating_Sizing_Factor, cooling_Sizing_Factor, HeatFuelSrc, BuiltUpArea, Climate,
                                   Occupancy, Typology, BuildingClass, lat, compliance):
    # variables : idd and available IDFs
    iddfile = "C:/EnergyPlusV8-6-0/Energy+.idd"
    # Standard IDF for Building Envelope
    IDF_SC = "LibraryFiles/ConstructionMasterFile.idf"
    IDF_SS = "LibraryFiles/ECBC_Office_Daytime Schedule.idf"
    # set idd file
    IDF.setiddname(iddfile)
    Proposed_IDF = IDF(IDF_P)
    status_msg = validate_proposed(Proposed_IDF, BuiltUpArea, compliance)
    if "Error" in status_msg:
        print "Invalid upload IDF!!"
        print status_msg
        return 1, "", Proposed_IDF, "", ""
        #return 1, "", Proposed_IDF, "", ""
    # step1 - Function call to create standard_ECBC_Idf from Proposed_Idf
    IDF_B = Create_Standard_IDF_From_Proposed(IDF_P)

    # configure all standard_libraries and IDFs
    ECBC_Construction_Library = IDF(IDF_SC)
    ECBC_Schedule_Library = IDF(IDF_SS)
    ECBC_standard_Idf = IDF(IDF_B)

    #


    # step2 - Read Required Info and Create Array
    # step 2.a - Read CSV file to generate zonelist wise schedules, lpd and system type list
    print "Reading Zonelist Wise Schedule..."
    ECBC_Zonelist, ECBC_Zonelist_Wise_Schedule = get_ECBC_Zonelist()
    # step 2.b - Read ECBC_Standard_Idf to generate zone and zonelist wise area, conditioned type, Design_Specification_Outdoor_Air_Object_Name, Design_Specification_Zone_Air_Distribution_Object_Name, story number
    print "Generating zone wise Building Story..."
    zone_wise_buildingstory_details = generate_Building_Story(ECBC_standard_Idf)
    # print zone_wise_buildingstory_details

    # step3 = Set Validation Check
    # step3.a - validated zonelist condition [idf should contain zonelist and name of zonelist should match ECBC zonelist name]
    status = zonelistCheck(ECBC_standard_Idf, ECBC_Zonelist)
    print status
    if "Invalid IDF" in status:
        # logic to remove created baseline idf
        os.remove(IDF_B)
        print "Standard IDF Generation failed"
        return 1, ECBC_standard_Idf, Proposed_IDF, "", ""
    # step3.b - Validity_check of e+ ( view-> validity check [ctrl+R]) not implemented

    # step4 a. - function call to Update Simulation, TimeStep, Sizing Period, RunPeriod and remove object list provided as csv
    ECBC_standard_Idf = fn_simulationControl(ECBC_standard_Idf, heating_Sizing_Factor, cooling_Sizing_Factor)

    # step4 b. - function call to Update outputControl_ReportingTolerance object in ECBC_standard Idf
    ECBC_standard_Idf = update_outputControl_ReportingTolerance(ECBC_standard_Idf)

    # step5 - Function call to updated Envelope Parameter:
    # a. remove shading ,
    # b. copy all Materials, construction from Construction_Library to ECBC_standard_Idf,
    # c.	Replace ECBC_Construction to Wall, Window, Roof, Skylight in ECBC_standard_Idf
    ECBC_standard_Idf, wall_list, roof_list = update_Envelope_Parameter(ECBC_Construction_Library, ECBC_standard_Idf,
                                                                        wall_U, roof_U, window_North_SHGC,
                                                                        window_NonNorth_SHGC, BuildingClass, lat, Climate, zone_wise_buildingstory_details)
    # step5.d -	Check and Update if WWR > 40 and SRR > 5
    print manage_WWR_SRR(ECBC_standard_Idf, wall_list, roof_list)
    ECBC_standard_Idf.save()

    # step6.	Lighting & Schedule
    # step6.a - function call to check Design_Level_Calculation_Method of light object should be 'Wats/Area' and  Update LPD in ECBC_standard_Idf
    ECBC_standard_Idf = update_lighting(ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule)
    ECBC_standard_Idf = remove_HVACTemplate_Schedule(ECBC_standard_Idf)
    # step6.b & c - function call to Copy Schedule_Library to ECBC_standard_Idf and Update schedule for People, Lights, Equipment from Schedule_Library
    ECBC_standard_Idf = update_schedule(ECBC_Schedule_Library, ECBC_standard_Idf, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    # step6.d - function call to set lighting control
    ECBC_standard_Idf = set_LightingControls(ECBC_standard_Idf, BuiltUpArea, ECBC_Zonelist_Wise_Schedule, Typology)

    # step7 -  Map_System [ not implemented]
    # step7.a -System mapping according to [zonelist, conditioned area, building type] [not implemented]
    ECBC_standard_Idf, appliedSystem, ECBC_SystemType_Library = System_type_assignment(ECBC_standard_Idf,
                                                                                       ECBC_Zonelist_Wise_Schedule,
                                                                                       zone_wise_buildingstory_details,
                                                                                       HeatFuelSrc, cooling_SetPoint)

    # Modifying Proposed idf - timestep, runperiod, outputControl_ReportingTolerance, Schedules
    Proposed_IDF = modify_Proposed(Proposed_IDF, ECBC_Schedule_Library, ECBC_Zonelist_Wise_Schedule, zone_wise_buildingstory_details)
    return 0, ECBC_standard_Idf, Proposed_IDF, appliedSystem, ECBC_SystemType_Library


def Generate_Baseline(IDF_P, wall_U, roof_U, window_North_SHGC, window_NonNorth_SHGC, cooling_SetPoint,
                      heating_Sizing_Factor, cooling_Sizing_Factor, HeatFuelSrc, WeatherFileName_Without_Extension,
                      BuiltUpArea, Climate, Occupancy, Typology, BuildingClass, lat, compliance):
    # pre processing  Function call
    status, ECBC_standard_Idf, Proposed_Idf, appliedSystem, ECBC_SystemType_Library = initialize_Baseline_Generation(
        IDF_P, wall_U, roof_U, window_North_SHGC, window_NonNorth_SHGC, cooling_SetPoint, heating_Sizing_Factor, cooling_Sizing_Factor, HeatFuelSrc, BuiltUpArea, Climate, Occupancy, Typology, BuildingClass, lat, compliance)
    #status = 0
    if status == 0:
        iddfile = "C:/EnergyPlusV8-6-0/Energy+.idd"
        IDF.setiddname(iddfile)

        # """
        # Run Simulation
        #appliedSystem = "C"  # delete
        print "starting simulation...."
        folder = os.path.dirname(IDF_P)

        #IDF_B=os.path.join(folder,"baseline.idf")  # delete
       # ECBC_standard_Idf = IDF(IDF_B) # delete
        if (os.path.exists(os.path.join(folder, "baseline.idf"))):

            # Proposed_Idf = IDF(IDF_P)  # delete
            # ECBC_SystemType_Library_Name = "SysC_HeatElect_EconN_HeatRecovN"   # delete
            # ECBC_SystemType_Library = IDF("./LibraryFiles/HVAC/" + ECBC_SystemType_Library_Name + ".idf")  # delete
            #folder = 'F:/PROJECTS/S1908_ECBC_Baseline_Generation_Automation/04_IC_T/03_Testing/Testing1'
            #Idf_Path_Without_Extension = os.path.join(folder, "baseline")
            Idf_Path_Without_Extension = os.path.join(folder.replace("/", "\\"), "baseline")
            print Idf_Path_Without_Extension
            result = runEplus(Idf_Path_Without_Extension, WeatherFileName_Without_Extension)
            #idfrun = IDF(Idf_Path_Without_Extension+".idf", WeatherFileName_Without_Extension+".epw")
            #result = idfrun.run( output_directory=folder, output_prefix="baseline", output_suffix="C")
            print result
            # POST RUN PROCESSING

            if result == None or result == 0:
                print "Sizing Run complete successfully!!"
                print "Post Processing...."
                Output_HTML = os.path.join(folder, "baselineTable.html")
                if appliedSystem.find("A") != -1:
                    print "Post processing for System A"
                    post_Run_Processing_System_A(Output_HTML, ECBC_standard_Idf)
                if appliedSystem.find("B") != -1:
                    print "Post processing for System B"
                    post_Run_Processing_System_B(Output_HTML, ECBC_standard_Idf)
                if appliedSystem.find("C") != -1:
                    print "Post processing for System C"
                    ECBC_standard_Idf = post_Run_Processing_System_C(Proposed_Idf, ECBC_standard_Idf, Output_HTML, ECBC_SystemType_Library)
                    ECBC_standard_Idf = Set_VAV_Fan_Efficiency(Output_HTML, ECBC_standard_Idf)
                if appliedSystem.find("D") != -1:
                    print "No Post processing for System D"
                ECBC_standard_Idf = Check_Econ_EnergyRecov_Applicabilty(ECBC_standard_Idf, BuiltUpArea, Climate,
                                                                        Occupancy, Typology)
            else:
                print "Error : Sizing Run not completed successfully!!"

        else:
            print ("Error: ECBC_standard_Idf not found")
        print ("Done!!!")


# # """
# #
# # # input proposed IDF
# # # note - no space in path
# # # IDF_P = "F:/TestBEP/180214_2/inStanZL_2.idf"
# # #IDF_P = "F:/TestBEP/180218_C/High-rise_revise_v8.idf"
# # #IDF_P = "F:/TestBEP/180308_4/SampleFile_3_v4C.idf"
# # # IDF_P = "F:/TestBEP/180216_AC/ITC_narmada_23_oversize8.idf"
# # # IDF_P = "F:/TestBEP/180218_B_ND/Sample_B.idf"
# # # IDF_P = "F:/TestBEP/Sample_Run/180219_C/Sample_C.idf"
# # # IDF_P = "F:/TestBEP/Sample_Run/180219_B1/Sample_B1.idf"
# # # IDF_P = "F:/TestBEP/180218_AC_ND/Sample_AC.idf"
# # # IDF_P = "F:/TestBEP/180222_1/Walmartidf_SysB_D.idf"
# # IDF_P = "F:/TestBEP/180222_2/Office(10-30).idf"
# # # IDF_P = "F:/TestBEP/180222_3/SampleFile_3_v4C.idf"
# IDF_P = "F:/TestBEP/180316/Sample_B1.idf"
# # Climate = raw_input('Specify climate: ')
# # typology = raw_input('Specify Typology: ')
#
# # get Baseline U value from database
# wall_U = 0.40
# roof_U = 0.33
# window_North_SHGC = 0.50
# window_NonNorth_SHGC = 0.27
# cooling_SetPoint = 25
# heating_Sizing_Factor = 1.25
# cooling_Sizing_Factor = 1.25
# HeatFuelSrc = "Elect"
# BuiltUpArea = 10000
# Climate = "WH"
# Occupancy = "Daytime"
# Typology = "Healthcare"
# BuildingClass = "Ed-Insti"  #"Ed-Sch" #"
# lat = 20
# compliance = 'ECBC'
# start_time = time.time()
#
# # Note: pass only name of weather file without extension and this should be in energyplus weather folder
# WeatherFileName = "IND_DL_New.Delhi-Safdarjung.AP.421820_ISHRAE2014"
# Generate_Baseline(IDF_P, wall_U, roof_U, window_North_SHGC, window_NonNorth_SHGC, cooling_SetPoint,
#                   heating_Sizing_Factor, cooling_Sizing_Factor, HeatFuelSrc, WeatherFileName, BuiltUpArea, Climate,
#                   Occupancy, Typology, BuildingClass, lat, compliance)
# print("--- %s seconds ---" % (time.time() - start_time))
# # output
