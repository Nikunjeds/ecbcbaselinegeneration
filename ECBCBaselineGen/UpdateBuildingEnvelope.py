"""helper function for updating Building Envelope details in IDF"""
from UvalExcep import checkException


# function to remove all shading objects
def removeShading(ECBC_standard_Idf):
    # read all shading object
    shading_objs = ECBC_standard_Idf.idfobjects["Shading:Building:Detailed".upper()]
    # for each shading
    length = len(shading_objs)
    for x in range(0, length):
        # pop shading element one by one
        ECBC_standard_Idf.popidfobject("Shading:Building:Detailed".upper(), 0)
    ECBC_standard_Idf.save()
    print "all Shading object removed"
    return ECBC_standard_Idf;


# function to copy objects from Library_IDF to ECBC_standard_Idf based on passed objectName
def copy_objects(L_IDF, S_IDF, objectName):
    obj_list = S_IDF.idfobjects[objectName.upper()]
    obj_names = [obj.Name for obj in obj_list]
    # search for passed object name in Library IDF
    library_objects = L_IDF.idfobjects[objectName]
    # for every Objects
    for i in range(len(library_objects)):
        library_objName = library_objects[i].Name
        # copy object if object name not exist in passed standard ECBC idf
        if not (library_objName in obj_names):
            ##copy standard material to ECBC_standard_Idf
            S_IDF.copyidfobject(library_objects[i])
            # print library_objects[i].Name + " Copied"
    # save ECBC_standard_Idf
    S_IDF.save()
    return S_IDF


# function to get standard Construction names based on u value of wall and roof, SHGC value of glazing
def get_Standard_Construction_Names(ECBC_standard_Idf, wall_U, roof_U, window_North_SHGC, window_NonNorth_SHGC):
    ##get wall, roof and glazing construction name
    construction_objs = ECBC_standard_Idf.idfobjects["Construction".upper()]
    # for every construction check for construction name that has ECBC_wall or ECBC_roof or ECBC_glazing in there name and store in variable
    for construction in construction_objs:
        if "ECBC_wall_" + str(wall_U) in construction.Name:
            ECBC_wall_construction = construction.Name
        if "ECBC_roof_" + str(roof_U) in construction.Name:
            ECBC_roof_construction = construction.Name
        if "ECBC_glazing_" + str(window_North_SHGC) in construction.Name:
            ECBC_window_construction_North_SHGC = construction.Name
        if "ECBC_glazing_" + str(window_NonNorth_SHGC) in construction.Name:
            ECBC_window_construction_NonNorth_SHGC = construction.Name
        if "ECBC_skylight" in construction.Name:
            ECBC_skylight_construction = construction.Name
    #    print ECBC_wall_construction
    #    print ECBC_roof_construction
    #    print ECBC_window_construction_North_SHGC
    #    print ECBC_window_construction_NonNorth_SHGC
    #    print ECBC_skylight_construction
    return ECBC_wall_construction, ECBC_roof_construction, ECBC_window_construction_North_SHGC, ECBC_window_construction_NonNorth_SHGC, ECBC_skylight_construction


# old function not used
# function to get list of waal and roof separately that will be used in assigning construction and checking WWR SRR
def get_Surface_List(ECBC_standard_Idf):
    roof_list = []
    wall_list = []
    surface_objs = ECBC_standard_Idf.idfobjects["BuildingSurface:Detailed".upper()]
    # for every surface, read Outside_Boundary_Condition and Surface_Type
    for surface in surface_objs:
        surface_nm = surface.Name
        # read surface Outside_Boundary_Condition and Surface_Type
        surface_outside_boundary = surface.Outside_Boundary_Condition
        surface_type = surface.Surface_Type
        # replace construction for all exterior Wall and roof
        if (surface_outside_boundary == "Outdoors"):  # exterior condition
            if (surface_type == "Wall"):
                wall_list.append(surface_nm)
            if (surface_type == "Roof"):
                roof_list.append(surface_nm)
    return roof_list, wall_list


# function to set standard construction name to surface and window
def set_Standard_Construction_Name(ECBC_standard_Idf, ECBC_wall_construction, ECBC_roof_construction,
                                   ECBC_window_construction_North_SHGC, ECBC_window_construction_NonNorth_SHGC,
                                   ECBC_skylight_construction, BuildingClass, lat, Climate, zone_wise_buildingstory_details):  # , roof_list, wall_list):
    ##search and replace construction
    ##replace construction name on surface: Outdoor Wall , roof, window, skylight
    roof_list = []
    wall_list = []

    print "Replace Construction in ECBC_standard_Idf"
    # read all surface details
    surface_objs = ECBC_standard_Idf.idfobjects["BuildingSurface:Detailed".upper()]
    # for every surface, read Outside_Boundary_Condition and Surface_Type
    for surface in surface_objs:
        surface_nm = surface.Name
        # read surface Outside_Boundary_Condition and Surface_Type
        surface_outside_boundary = surface.Outside_Boundary_Condition
        surface_type = surface.Surface_Type
        # replace construction for all exterior Wall and roof
        # also create list of wall and roof names
        if (surface_outside_boundary == "Outdoors"):  # exterior condition
            if (surface_type == "Wall"):
                wall_list.append(surface_nm)
                surface.Construction_Name = ECBC_wall_construction

                # print surface.Construction_Name + " updated in " + surface.Name
            if (surface_type == "Roof"):
                roof_list.append(surface_nm)
                surface.Construction_Name = ECBC_roof_construction

                # print surface.Construction_Name + " updated in " + surface.Name

    # replace fenestration construction name
    # Read north_axis from Building object
    building_obj = ECBC_standard_Idf.idfobjects["Building".upper()][0]
    north_axis = building_obj.North_Axis
    # print north_axis
    # read all fenestration details
    fenestration_objs = ECBC_standard_Idf.idfobjects["FenestrationSurface:Detailed".upper()]
    # for every fenestration - get azimuth of window and north axis and check applicable construction name(North, Non-North)
    for fenestration in fenestration_objs:
        if fenestration.Building_Surface_Name in wall_list:
            azimuth = fenestration.azimuth + north_axis
            if azimuth > 360:
                azimuth = azimuth - 360
            # if final azimuth is between 45 to 315.. set SHGC_North Construction else set SHGC_Non_North
            if (azimuth <= 45 or azimuth >= 315):
                fenestration.Construction_Name = ECBC_window_construction_North_SHGC
            else:
                fenestration.Construction_Name = ECBC_window_construction_NonNorth_SHGC

            # print "azimuth "+ str(azimuth)
            # print fenestration.Construction_Name + " updated in " + fenestration.Name + " having Azimuth " + str(azimuth)
        if fenestration.Building_Surface_Name in roof_list:
            fenestration.Construction_Name = ECBC_skylight_construction

            # print fenestration.Construction_Name + " updated in " + fenestration.Name + " skylight"

    ECBC_standard_Idf.save()
    checkException(ECBC_standard_Idf, BuildingClass, Climate, lat, north_axis, zone_wise_buildingstory_details)
    return ECBC_standard_Idf, wall_list, roof_list


# main fuction to update Envelope Details..
def update_Envelope_Parameter(ECBC_Construction_Library, ECBC_standard_Idf, wall_U, roof_U, window_North_SHGC,
                              window_NonNorth_SHGC, BuildingClass, lat, Climate, zone_wise_buildingstory_details):
    ECBC_standard_Idf = removeShading(ECBC_standard_Idf)
    # copy Materials to ECBC_standard_Idf
    print "Copying Material in ECBC_standard_Idf..."
    ECBC_standard_Idf = copy_objects(ECBC_Construction_Library, ECBC_standard_Idf, "MATERIAL".upper())
    # copy window Materials to ECBC_standard_Idf
    print "Copying Window Material in ECBC_standard_Idf..."
    ECBC_standard_Idf = copy_objects(ECBC_Construction_Library, ECBC_standard_Idf,
                                     "WindowMaterial:SimpleGlazingSystem".upper())
    # copy construction to ECBC_standard_Idf
    print "Copying Construction in ECBC_standard_Idf..."
    ECBC_standard_Idf = copy_objects(ECBC_Construction_Library, ECBC_standard_Idf, "Construction".upper())

    ECBC_standard_Idf.save()

    # Function call to get Standard construction name
    ECBC_wall_construction, ECBC_roof_construction, ECBC_window_construction_North_SHGC, ECBC_window_construction_NonNorth_SHGC, ECBC_skylight_construction = get_Standard_Construction_Names(
        ECBC_standard_Idf, wall_U, roof_U, window_North_SHGC, window_NonNorth_SHGC)

    # function call to get all exterior rool and wall names
    # roof_list, wall_list = get_Surface_List(ECBC_standard_Idf)
    # print roof_list
    # print wall_list

    # function to set Standard construction name in wall, roof, window, skylight
    ECBC_standard_Idf, wall_list, roof_list = set_Standard_Construction_Name(ECBC_standard_Idf, ECBC_wall_construction,
                                                                             ECBC_roof_construction,
                                                                             ECBC_window_construction_North_SHGC,
                                                                             ECBC_window_construction_NonNorth_SHGC,
                                                                             ECBC_skylight_construction, BuildingClass,
                                                                             lat, Climate, zone_wise_buildingstory_details)  # , roof_list, wall_list)

    return ECBC_standard_Idf, wall_list, roof_list
